"""
title: ddtree.py

language: python 3.5

author: bue

coauthor: Carly

description:
  dictionary tree object implementation.

run::

  from pyddtree.ddtree import ddtree

"""

# libraries
import copy
import csv
import json
import re
import sys

# constants
ts_none = ('NONE','None','none','Null','Null','null', '')  #'empty'


# calx functions
def tsv_writeheader(o_ddtree, d_calxargs=None):
    """
    description:
      *internal method*
      called from tsv_writeleaflisting or tsv_writeleafinline.
      makes a new tsv file and writes header row.
    """
    # set variables
    o_ddtree.b_header = True
    ls_header = []

    # extract d_calxargs, get leaf header
    ls_leaf = list(o_ddtree.d_leaf.keys())
    if (d_calxargs["ls_veinpop"] != None):
        for s_veinpop in d_calxargs["ls_veinpop"]:
            i_veinpop = ls_leaf.index(s_veinpop)
            ls_leaf.pop(i_veinpop)
    ls_leaf.sort()

    # get branch and twig header
    if (o_ddtree.ls_bark != None):
        assert len(o_ddtree.ls_bark) == len(o_ddtree.ls_branch), "o_ddtree.ls_bark {}, \
set via o_ddtree.set_bark(), must have same length as o_ddtree.ls_branch {}, \
which length dependent on the leaf layer, set via o_ddtree.set_leaf().".format(o_ddtree.ls_bark, o_ddtree.ls_branch)
        ls_header = o_ddtree.ls_bark
    elif (o_ddtree.s_ddtree != None):
        # get branch part
        tl_blueprint = o_ddtree.parse_ddtree()
        llsi_branchheader = tl_blueprint[0]
        for lsi_branchheader in llsi_branchheader:
            s_branchheader = lsi_branchheader[0]
            ls_header.append(s_branchheader)
        # get twig part
        if (o_ddtree.b_twig):
            ls_header.append("twig")
    else:
        # get branch twig part
        for i in range(len(o_ddtree.ls_branch)):
            s_header = "branch" + str(i)
            ls_header.append(s_header)

    # fuse header output
    ls_header.extend(ls_leaf)

    # populate output file
    with open(o_ddtree.s_ofile, 'w') as f_tsv:  # newline=''
        o_writer = csv.writer(f_tsv, delimiter='\t')  # quotechar='"', quoting=csv.QUOTE_MINIMAL
        o_writer.writerow(ls_header)


def tsv_writeleaflisting(o_ddtree, d_calxargs=None):
    """
    description:
      *internal method*,
      called via calx from tsv_write.
      write any leaf vein list as a row by row listing.
      this is a pandas and R dataframe and excel compatible flat file format.
    """
    # handle header
    if not (o_ddtree.b_header):
        tsv_writeheader(o_ddtree=o_ddtree, d_calxargs=d_calxargs)

    # get leaf vein keys
    ls_leaf = list(o_ddtree.d_leaf.keys())
    if (d_calxargs["ls_veinpop"] != None):
        for s_veinpop in d_calxargs["ls_veinpop"]:
            i_veinpop = ls_leaf.index(s_veinpop)
            ls_leaf.pop(i_veinpop)
    ls_leaf.sort()

    # set output
    ll_branchleaf = []

    # handle leaf
    ll_column = []
    i_row = 1
    # get columns
    for s_vein in ls_leaf:
        #if s_vein in d_calxargs["ls_vein"]:
        o_vein = o_ddtree.d_leaf[s_vein]
        # stacked twig leaf
        if (isinstance(o_vein,dict)):
            l_column = ["{leaf}"]
        # list of values
        elif (isinstance(o_vein,list)):
            l_column = o_vein
            # handle list and list of list length
            if (len(l_column) == i_row):
                pass
            elif (len(l_column) < i_row):
                i_gap = i_row - len(l_column)
                l_gap =  [None] * i_gap
                l_column.extend(l_gap)
            else:
                # (len(l_column) > i_row)
                i_gap = len(l_column) - i_row
                i_row = len(l_column)
                for l_handle in ll_column:
                    if (len(set(l_handle)) < 1):
                        l_gap = [None] * i_gap
                    else:
                        l_gap = [l_handle[0]] * i_gap
                    l_handle.extend(l_gap)
        # single value
        else:
            l_column = [o_vein]
            if (i_row > 1):
                l_column = l_column * i_row
        # populate list of column
        ll_column.append(l_column)
    # transpose and extend list of columns
    ll_transpose = copy.deepcopy(ll_column)
    while (len(ll_transpose[0]) > 0):
        l_branchleaf = o_ddtree.ls_branch.copy()
        l_leaf = []
        for l_column in ll_transpose:
            o_pop = l_column.pop(0)
            l_leaf.append(o_pop)
        # populate output
        l_branchleaf.extend(l_leaf)
        ll_branchleaf.append(l_branchleaf)

    # write to file
    with open(o_ddtree.s_ofile, 'a') as f_tsv:  # newline=''
        o_writer = csv.writer(f_tsv, delimiter='\t')  # quotechar='"', quoting=csv.QUOTE_MINIMAL
        o_writer.writerows(ll_branchleaf)
    # output
    return(None)


def tsv_writeleafinline(o_ddtree, d_calxargs=None):
    """
    description:
      *internal method*,
      called via calx from tsv_write.
      write any leaf vein list inline into one tab separated field.
      pandas and R dataframe and excel can not really handle this flat file format.
    """
    # handle header
    if not (o_ddtree.b_header):
        tsv_writeheader(o_ddtree=o_ddtree, d_calxargs=d_calxargs)

    # get leaf vein keys
    ls_leaf = list(o_ddtree.d_leaf.keys())
    if (d_calxargs["ls_veinpop"] != None):
        for s_veinpop in d_calxargs["ls_veinpop"]:
            i_veinpop = ls_leaf.index(s_veinpop)
            ls_leaf.pop(i_veinpop)
    ls_leaf.sort()

    # handle data row
    s_branch = "\t".join(o_ddtree.ls_branch)
    s_leaf = None
    for s_vein in ls_leaf:
        if (s_leaf == None):
            s_leaf = json.dumps(o_ddtree.d_leaf[s_vein])
        else:
            s_leaf = s_leaf + "\t" + json.dumps(o_ddtree.d_leaf[s_vein])

    # fuse row
    s_out = s_branch + "\t" + s_leaf + "\n"

    # append file data row
    f = open(o_ddtree.s_ofile, 'a')
    f.write(s_out)

    # close output file
    f.close()
    return(None)


# main class
class ddtree(dict):
    """
    description:
        dictionary of dictionary tree object class
    """

    def __init__(self):
        """
        description:
          the seed.
          the initialization method to initialize
          an empty dictionary of dictionary tree.

        run::

          o_sequoia = ddtree()

        """
        self.set_bark()
        self.set_ddtree()
        self.set_development()
        self.set_ifile()
        self.set_leaf()
        self.set_ofile()
        self.set_png()
        self.set_twig()
        self.set_verbose()


    def set_bark(self, ls_bark=None):
        """
        description:
          set explicit branch labels.
          this labels will be used by self.tsv_write() to name
          the branch column. default is None, which will result
          in [branch0, branch1, ...] type of ls_bark.

        input:
          - ls_bark: tsv branch column label list of strings

        output:
          - self.ls_bark

        run::

          self.set_bark(["foo1","foo2"])

        """
        if not hasattr(self, "ls_bark"):
            self.ls_bark = None
        self.ls_bark = ls_bark


    def set_ddtree(self, s_ddtree=None):
        """
        description:
          set ddtree string used by self.tsv_read() to read out data files,
          to populate the ddtree object, to grow the tree.
          a more detailed description of the s_ddtree syntax can be found at the
          self.parse_ddtree docstring on this page.

        input:
          - s_ddtree: ddtree syntax string.
            this string specifies how to read out the tsv file into ddtree object.

        output:
          - self.s_ddtree

        run::

          self.s_ddtree("{branch0:{branch1:{(twig:vein0),(twig:vein1),(twig:vein2:True)}}}")

        """
        if not hasattr(self, "s_ddtree"):
            self.s_ddtree = None
        self.s_ddtree = s_ddtree


    def set_development(self, b_development=False):
        """
        description:
          boolean.
          if set true, ddtree tsv_read will load at max 3 values per vein.
          and print at max one plot. resetting b_development (by True or False)
          will reset the plot output counter what so ever.
          default is False.

        input:
          - b_development: True or False

        output:
          - self.b_development
          - self.b_outta: *internal variable*
            set to False as soon as one png plot is generated.

        run:

          self.set_development(True)

        """
        if not hasattr(self, "b_development"):
            self.b_development = None
        self.b_development = b_development
        self.b_outta = True


    def set_ifile(self, s_ifile=None):
        """
        description:
          set input path and filename

        input:
          - s_ifile: "/path/input.file"

        output:
          - self.s_ifile

        run::

          self.set_ifile("input.json")

        """
        if not hasattr(self, "s_ifile"):
            self.s_ifile = None
        self.s_ifile = s_ifile


    def set_leaf(self, s_vein=None):
        """
        description:
          set vein label to make the leaf layer detectable..
          this means each, every and only the leaf constructs have to have a vein labeled like this.

        input:
          - s_vein: leaf vein label.

        output:
          - self

        run::

          self.set_leaf("foo")

        """
        if not hasattr(self, "s_vein"):
            self.s_vein = None
        self.s_vein = s_vein


    def set_ofile(self, s_ofile=None):
        """
        description:
          set output path and filename

        input:
          - s_ifile: "/path/output.file"

        output:
          - self.s_ofile

        run::

          self.set_ofile("output.json")

        """
        if not hasattr(self, "s_ofile"):
            self.s_ofile = None
        self.s_ofile = s_ofile


    def set_png(self, b_png=False):
        """
        description:
          boolean to enable for plots png output rather then screen output.
          default is False.

        input:
          - b_png: True or False.

        output:
          - self.b_png

        run::

          self.set_png(True)

        """
        if not hasattr(self, "b_png"):
            self.b_png = None
        self.b_png = b_png


    def set_verbose(self, b_verbose=False):
        """
        description:
          boolean to enable verbose standard output.
          None gives no output and is e.g. used by  unittests.
          False gives some output.
          True is most verbose and as such ideal for debugging.
          default is False.

        input:
          - b_verbose: True or False

        output:
          - self.b_verbose

        run::

          self.set_verbose(True)

        """
        if not hasattr(self, "b_verbose"):
            self.b_verbose = None
        self.b_verbose = b_verbose


    def set_twig(self, b_twig=False):
        """
        description:
          boolean. if true tsv_writeleaflisting or tsv_writeleafinline
          will have a column named twig in the header rigth after the branches
          extracted form the s_ddtree string.
          default is False.

        input:
          - b_twig: True or False

        output:
          - self.b_twig

        run::

          self.set_twig(True)

        """
        if not hasattr(self, "b_twig"):
            self.b_twig = None
        self.b_twig = b_twig


    def leaf0kcore(self, ls_key):  # d_leaf, ls_branch,
        """
        description:
          *internal method*, self.leaf0k() specific turingtape method part.
        """
        # on leaf
        es_vein = set(ls_key)
        if (self.es_vein == None):
            self.es_vein = es_vein
        else:
            if (self.es_vein != es_vein):
                sys.exit("Error: inconsistnet leaf vein {}\n at {}\n against leaf vein set {}.".format(self.es_vein.symmetric_difference(es_vein), self.ls_branch, self.es_vein))


    def getcore(self, ls_branchtwigleaf):  # d_leaf, ls_branch,
        """
        description:
          *internal method*, self.get() specific turingtape method part.
        """
        # on ls_branchtwigleaf?
        if (ls_branchtwigleaf == None):
            b_get = True
        else:
            # and gate
            b_get = False
            for s_wood in ls_branchtwigleaf:
                if s_wood in self.ls_branch:
                    b_get = True
                else:
                    b_get = False
                    break

        # get leaf vein values
        if b_get:
            o_result = self.d_leaf[self.s_vein]  # s_input
            if (self.l_get == None):
                self.l_get = []
            if (type(o_result) == list):  # only list, neither set nor tuple
                self.l_get.extend(o_result)
            else:
                self.l_get.append(o_result)

        if self.b_verbose:
            print("Get leaf:", b_get, self.ls_branch, self.s_vein, self.l_get)


    def update_branch(self):  # d_leaf, ls_branch
        """
        description:
          *internal method*, called from self.calx(), self.delete(),
          self.put(), self.rename(), self.twigpop(), self.tsv_read().
          update ddtree at self.ls_branch with d_leaf
          works because of call by reference
        """
        # only twigleaf
        if (len(self.ls_branch) < 1):
            self = self.d_leaf
        # branch and twigleaf
        else:
            # update branching
            s_twig = self.ls_branch[-1] # save twig
            d_fractal = self
            for s_branch in self.ls_branch:
                d_twigleaf = d_fractal  # save possible twigleaf
                try:
                    d_fractal = d_fractal[s_branch]
                except KeyError:
                    d_fractal.update({s_branch : {}})
                    d_fractal = d_fractal[s_branch]
            # update twig leaf
            d_twigleaf.update({s_twig : self.d_leaf})


    def deletecore(self, ls_branchtwigleaf):  # d_leaf, ls_branch
        """
        description:
          *internal method*, self.delete() specific turingtape method part.
        """
        # on ls_branchtwigleaf?
        if (ls_branchtwigleaf == None):
            b_delete = True
        else:
            # and gate
            b_delete = False
            for s_wood in ls_branchtwigleaf:
                if s_wood in self.ls_branch:
                    b_delete = True
                else:
                    b_delete = False
                    break
        if self.b_verbose:
            print("delete vein :", b_delete, self.ls_branch, self.s_vein)

        # write None as vein value to leaf
        if b_delete:
            #d_leaf.update({self.s_vein : None})
            self.d_leaf.pop(self.s_vein)
            self.update_branch()  # d_leaf=d_leaf, ls_branch=ls_branch


    def putcore(self, s_vein, o_value, ls_branchtwigleaf, b_overwrite):  #  d_leaf, ls_branch
        """
        description:
          *internal method*, self.put() specific turingtape method part.
        """
        # on ls_branchtwigleaf?
        if (ls_branchtwigleaf == None):
            b_put = True
        else:
            # and gate
            b_put = False
            for s_wood in ls_branchtwigleaf:
                if s_wood in self.ls_branch:
                    b_put = True
                else:
                    b_put = False
                    break
        if self.b_verbose:
            print("Put vein :", b_put, self.ls_branch, s_vein, o_value)

        # write vein value to leaf
        if b_put:
            if b_overwrite:
                try:
                    o_fruit = self.d_leaf[s_vein]
                    sys.exit("Error: can not put {} {} at {}. found value {} {}. and b_overwrite is set to {}.".format(s_vein, o_value, self.ls_branch, s_vein, o_fruit, b_overwrite))
                except KeyError:
                    pass
            self.d_leaf.update({s_vein : o_value})
            self.update_branch()  # d_leaf=d_leaf, ls_branch=ls_branch


    def renamecore(self, s_vein, b_overwrite):  # d_leaf, ls_branch
        """
        description:
          *internal method*, self.rename() specific turingtape method part.
        """
        # handle leaf
        # get write permission
        b_write = b_overwrite
        if not b_overwrite:
            try:
                self.d_leaf[s_vein]
                # nop
                if (self.b_verbose != None):
                    print("NOP: at {} s_vein {} already exist, b_overwrite set to {}.".format(self.ls_branch, s_vein, b_overwrite))
            except KeyError:
                b_write = True

        # write leaf
        if b_write:
            # rename
            o_value = self.d_leaf[self.s_vein]
            self.d_leaf.update({s_vein : o_value})
            self.d_leaf.pop(self.s_vein)
            self.update_branch()  # d_leaf=d_leaf, ls_branch=ls_branch


    def twigpopcore(self, s_vein):  # d_leaf, ls_branch,
        """
        description:
          *internal method*, self.twigpopcore() specific turingtape method part.
        """
        # on future twig leaf
        # pop twigs
        for s_twig in list(self.d_leaf.keys()):
            o_value = self.d_leaf[s_twig][s_vein]
            self.d_leaf[s_twig] = o_value

        # fuse new twig leaf
        self.update_branch()  # d_leaf=d_leaf, ls_branch=ls_branch


    def calxcore(self, o_calx, d_calxargs, ls_branchtwigleaf):  # d_leaf, ls_branch
        """
        description:
          *internal method*, self.calxcore() specific turingtape method part.
        """
        # bue: this is hardcore
        # on ls_branchtwigleaf?
        if (ls_branchtwigleaf == None):
            b_calx = True
        else:
            # and gate
            b_calx = False
            for s_wood in ls_branchtwigleaf:
                if s_wood in self.ls_branch:
                    b_calx = True
                else:
                    b_calx = False
                    break
        if self.b_verbose:
            print("Calx on leaf :", b_calx, self.ls_branch)

        # get leaf values
        if b_calx:
            # fuse calx arguments
            # branch and leaf is always part of the args
            # bue 20150130: evt establish self.d_leaf and self.ls_branch
            #if (d_calxargs == None):
            #    d_calxargs = {"d_leaf":d_leaf, "ls_branch":ls_branch}
            #else:
            #    d_calxargs.update({"d_leaf":d_leaf, "ls_branch":ls_branch })

            # get calculation
            o_result = o_calx(self, d_calxargs=d_calxargs)

            # write to leaf
            if (o_result != None):
                if (o_result in ts_none):
                    o_result = None
                self.d_leaf.update({o_calx.__name__ : o_result})
                self.update_branch()  # d_leaf=d_leaf, ls_branch=ls_branch


    def turingtape(self, s_function=None, o_calx=None, d_calxargs=None, o_value=None, s_vein=None, ls_branchtwigleaf=None, b_overwrite=False):
        """
        description:
          this function pares the whole tree, starting from the root,
          visits each single leaf.
          this is the hard core of the ddtree class.
          the idea to do this implementation came by Jeff Ulman's automata course,
          taken in autumn 2015 at coursera. towards the end of the course i spent some
          sunny holiday days in San Francisco, visiting my friends Carly and Trevor
          from urologydx.com. this was where it all started.
        """
        # bue: this is hardcore
        # handle input
        if (self.s_vein == None):
            sys.exit("Error: no leaf layer specified. run self.set_leaf('some_vein') first.")

        # initialize stack tape and branch path tape
        ls_stack = ["#root#"]  # stack tape
        self.ls_branch = []  # brach path tape
        s_key = None
        #d_leaf = self
        self.d_leaf = self  # bue 20160722: have evt to be copied or even deep copied
        ld_stack = [self.d_leaf]  # list of dictionaries
        ls_key = list(self.d_leaf.keys())
        if self.b_verbose:
            print("\nInitialization:")
            print("Stack :", ls_stack)
            print("Branch :", self.ls_branch)
            print("Key :", s_key)
            print("Ls_Key :", ls_key)
            #print("ld_Stack:", ld_stack)

        while (len(ls_stack) > 0):
            # process

            # on leaf
            if (self.s_vein in ls_key):  # value

                # calx core routine
                if (s_function == "calx"):
                    self.calxcore(o_calx=o_calx, d_calxargs=d_calxargs, ls_branchtwigleaf=ls_branchtwigleaf)  # d_leaf=d_leaf, ls_branch=ls_branch
                # check core routine
                elif (s_function == "leaf0k"):
                    self.leaf0kcore(ls_key=ls_key)  # d_leaf=d_leaf, ls_branch=ls_branch,
                # manipu core routine
                elif (s_function == "delete"):
                    self.deletecore(ls_branchtwigleaf=ls_branchtwigleaf)  # d_leaf=d_leaf, ls_branch=ls_branch
                elif (s_function == "get"):
                    self.getcore(ls_branchtwigleaf=ls_branchtwigleaf)  # d_leaf=d_leaf, ls_branch=ls_branch
                elif (s_function == "put"):
                    self.putcore(s_vein=s_vein, o_value=o_value, ls_branchtwigleaf=ls_branchtwigleaf, b_overwrite=b_overwrite)  # d_leaf=d_leaf, ls_branch=ls_branch
                elif (s_function == "rename"):
                    self.renamecore(s_vein=s_vein, b_overwrite=b_overwrite)  # d_leaf=d_leaf, ls_branch=ls_branch
                elif (s_function == "twigpop"):
                    self.twigpopcore(s_vein=s_vein)  # d_leaf=d_leaf, ls_branch=ls_branch
                # not yet implemented core routine
                else:
                    sys.exit("Error: unknown turingtape function {}.".format(o_function))

                # handle stack
                if (len(ls_stack) != 1):
                    self.ls_branch.pop(-1)
                else:
                    # one leaf tree case
                    ls_stack.append("#fork#")
                    self.ls_branch.append("#root#")

            # on branch
            else:
                # handle d stack
                for s_key in ls_key:
                    ld_stack.append(self.d_leaf[s_key])
                # handle s stack
                ls_key.insert(0, "#fork#")
                ls_stack.extend(ls_key)

            # process branch
            s_key = ls_stack.pop(-1)
            self.d_leaf = ld_stack.pop(-1)
            # 20160423 bue: this is maybe a really bad hack
            # 20160506 bue: it tuns out to be a really cool hack
            b_saw = True
            while b_saw:
                try:
                    ls_key = list(self.d_leaf.keys())
                    b_saw = False
                except AttributeError:
                    s_key = ls_stack.pop(-1)
                    self.d_leaf = ld_stack.pop(-1)

            # on fork ?
            while (s_key == "#fork#"):
                s_key = ls_stack.pop(-1)
                # not on root ?
                if (s_key != "#root#"):
                    self.ls_branch.pop(-1)

            # update branch
            self.ls_branch.append(s_key)

            # output
            if self.b_verbose:
                print("\nStack :", ls_stack)
                print("Branch :", self.ls_branch)
                print("Key :", s_key)
                print("Ls_Key :", ls_key)
                #print("Ld_Stack :", ld_stack)
                #print("Self :", self)


    def leaf0k(self):
        """
        description:
          check leaf layer, specified by self.s_vein, for consistent.
          this is especially valuable when leafs were manipulated e.g. by
          self.put(), self.rename(), self.twigpop().

        input:
          - self.es_vein: if leaf layer is ok, the consistent s_vein set.

        output:
          - self.b_leaf0k: True or False.
          - Error if any leaf structure is different then the one of the other leafs.

        run::

          self.set_leaf("foo")
          self.leaf0k()

        """
        # initialize variable
        self.es_vein = None
        # call turing tape main function
        self.turingtape(s_function="leaf0k")
        # output
        return(self.es_vein)


    def delete(self, ls_branchtwigleaf=None):
        """
        description:
          set each leaf vein, specified by self.s_vein and ls_branchtwigleaf,
          to None.

        input:
          - self.s_vein: vein which specifies the leaf layer.
            set vein via self.set_leaf()
          - ls_branchtwigleaf: branch or twig to specifically set vein values
            to None. the list elements are boolean AND evaluated to decide
            if the vein values should be set to None or left alone.
            default ls_branchtwigleaf is None, which will set all vein value
            specified by self.s_vein to None.

        output:
          - self

        run::

          self.set_leaf("foo")
          self.delete()
          self.delete(["fruit_tree"])
          self.delete(["nivalis"])
          self.delete(["fruit_tree","pear","nivalis"])

        """
        # initialize variable
        #self.b_delete = True # bue 20160716: makes no sense
        b_out = False
        # call turing tape main function
        self.turingtape(s_function="delete", ls_branchtwigleaf=ls_branchtwigleaf)
        # output
        b_out = True
        return(b_out)



    def get(self, ls_branchtwigleaf=None):
        """
        description:
          get self.s_vein values, specified by self.s_vein, from all
          or ls_branchtwigleaf specified branches.

        input:
          - self.s_vein: vein to get the values from. set via self.set_leaf()
          - ls_branchtwigleaf: branch or twig to get vein values from.
            the list elements are boolean AND evaluated to decide
            if values are picked or not. default ls_branchtwigleaf is None,
            which will get all self.s_vein specified vein values from the tree.

        output:
          - self.l_get: list of found values.

        run::

          self.set_leaf("foo")
          self.get()
          self.get(["fruit_tree"])
          self.get(["nivalis"])
          self.get(["fruit_tree","pear","nivalis"])

        """
        # initialize variable
        self.l_get = None
        # call turing tape main function
        self.turingtape(s_function="get", ls_branchtwigleaf=ls_branchtwigleaf)
        # output
        return(self.l_get)


    def put(self, s_vein, o_value, ls_branchtwigleaf=None, b_overwrite=False):
        """
        description:
          put {s_vein: o_value} to each leaf specified by self.s_vein and
          ls_branchtwigleaf.

        input:
          - self.s_vein: vein which specifies the leaf layer.
            set vein via self.set_leaf()
          - ls_branchtwigleaf: branch or twig to put vein value pair.
            the list elements are boolean AND evaluated to decide
            if the vein values pair is put there or not.
            default ls_branchtwigleaf is None, which will put the vein value
            pair to all leafs on the leaf layer specified by self.s_vein.
          - b_overwrite: boolen. if True self.put() will overwrite values
            at self.s_vein without warning. if False self.put() will break
            by error, if on a leaf this vein already exist. default is False.

        output:
          - self

        run::

          self.set_leaf("foo")
          self.put(s_vein="count", o_value=42)
          self.put(s_vein="count", o_value=42, ls_branchtwigleaf=["nivalis"])
          self.put(s_vein="count", o_value=[42,None,64,32,16], ls_branchtwigleaf=["nivalis"])

        """
        # initialize variable
        b_out = False
        # call turing tape main function
        self.turingtape(s_function="put", s_vein=s_vein, o_value=o_value, ls_branchtwigleaf=ls_branchtwigleaf, b_overwrite=b_overwrite)
        # output
        b_out = True
        return(b_out)


    def rename(self, s_vein, b_overwrite=False):
        """
        description:
          renames self.s_vein specified vein by s_vein.

        input:
          - self.s_vein: vein to be renamed. set via self.set_leaf()
          - s_vein: new vein name
          - b_overwrite: boolen. if True self.rename() will overwrite values
            at s_vein veins without warning. if False self.rename() will not do
            any operation on the vein, but spit out a warning message.

        output:
          - self

        run::

          self.set_leaf("foo")
          self.rename("foo_figthers")
          self.rename(s_vein="foo_figthers", b_overwrite=True)

        """
        # initialize variable
        b_out = False
        # call turing tape main function
        self.turingtape(s_function="rename", s_vein=s_vein, b_overwrite=b_overwrite)
        # output
        b_out = True
        return(b_out)


    def twigpop(self, s_vein):
        """
        description:
            will take s_vein values. pops all veins and fuses the values to the
            consecutive twig, specified by self.s_vein via self.set_leaf()
            by this action you will lose all other veins values of the leaf.

        input:
          - s_vein: leaf vein with the values of interest
          - self.s_vein: twig which will become the new vein,
            set via self.set_leaf()

        output:
          - self

        run input:

        | {
        |   "fruit_tree": {
        |     "pear": {
        |       "nivalis": {
        |         "count": [42,None,64,32,16],
        |         "colour": "emerald",
        | }}}}

        code::

          self.set_leaf("nivalis")
          self.twigpop("count")

        result:

        | {
        |   "fruit_tree": {
        |     "pear": {
        |       "nivalis": [42,None,64,32,16],
        | }}}}

        """
        # initialize variable
        b_out = False
        #self.b_twigpop = True # bue 20160716: makes no sense
        # call turing tape main function
        self.turingtape(s_function="twigpop", s_vein=s_vein)
        # output
        b_out = True
        return(b_out)


    def calx(self, o_calx, d_calxargs=None, ls_branchtwigleaf=None):
        """
        description:
            call o_calx method, which work natively on ddtree constructs,
            at all or specified ls_branchtwigleaf branches.
            o_calx methods can be loaded via:
            from pyddtree import calx ( mathematical functions ) and
            from pyddtree import pixie(plot functions).
            results from the function will either be added as additional
            vein value pare to each leaf or plotted to the screen or a png file.

        input:
          - o_calx: mathematical or plot methods function
          - d_calxargs: dictionary to specify o_calx related,
            required or optional arguments.
          - ls_branchtwigleaf: branch to get o_calx working.
            the list elements are boolean AND evaluated to decide
            if o_calx will be applied to this leaf.
            default is None which will execute o_calx on all leafs
            at the leaf layer, specified by self.s_vein.

        output:
          - self

        run::

          from pyddtree import calx

          self.set_leaf("foo")
          self.calx(calx.log2)
          self.calx(o_calx=calx.log2, ls_branchtwigleaf=["nivalis"])

          d_calxargs = {}
          d_calxargs.update({"r_low": -3})
          d_calxargs.update({"r_high": 3})
          self.set_leaf(calx.s4mirror)   # uses default r_low and r_high values
          self.set_leaf(o_calx=calx.s4mirror, ls_branchtwigleaf=["nivalis"])  # uses default r_low and r_high values
          self.set_leaf(o_calx=o_calx.s4mirror, d_calxargs=d_calxargs)
          self.set_leaf(o_calx=o_calx.s4mirror, d_calxargs=d_calxargs, ls_branchtwigleaf=["nivalis"])

        """
        # bue: this is hardcore
        # initialize variable
        b_out = False
        # call calx turing tape main function
        self.turingtape(s_function="calx", o_calx=o_calx, d_calxargs=d_calxargs, ls_branchtwigleaf=ls_branchtwigleaf)
        # output
        b_out = True
        return(b_out)



    def json_read(self):
        """
        description:
          read ddtree.json file specified by self.set_ifile()
          via standard python json library.

        input:
           - self.s_ifile: input path, filename and extension

        output:
           - self

        run::

          self.set_ifile("/path/input.json")
          self.json_read()

        """
        if (self.b_verbose != None):
            print("\nProcess read_json ...")
            print("Read input file: {}".format(self.s_ifile))
        with open(self.s_ifile, newline='') as f_json:
            d_json = json.load(f_json)
            self.update(d_json)
        # output
        f_json.close()
        if (self.b_verbose != None):
            print("0K")


    def json_write(self):
        """
        description:
          write self.s_ofile ddtree.json file
          via stanadard python json libray.
          self.s_ofile can be specified via self.set_ofile().

        input:
           - self.s_ofile: output path, filename and extension

        output:
           - ddtree.json: file

        run:

          self.set_ofile("/path/output.json")
          self.json_write()

        """
        # read file with standard python json library
        if (self.b_verbose != None):
            print("\nProcess write_json ...")
            print("Write output file: {}".format(self.s_ofile))
        with open(self.s_ofile, 'w', newline='') as f_json:
            d_json = json.dump(self, f_json)  #indent=2
        # output
        f_json.close()
        if (self.b_verbose != None):
            print("0K")


    def xray(self, do_skeleton=None):
        """
        description:
          peels away the leaf vein's values.
          handy to study the ddtree structure without getting interfered
          by the bulk of values.
          this function is dedicated to Chrisie.

        input:
          - self (or another dictionary of dictionary tree object)

        output:
          - do_skeleton: dictionary of dictionary tree object

        run::

          import json
          dd_skeleton = self.xray()
          json.dumps(dd_skeleton, indent=2)

        """
        # clone
        if (do_skeleton == None):
            do_skeleton = copy.deepcopy(self)
        # x ray
        ls_key = list(do_skeleton.keys())
        for s_key in ls_key:
            o_subtree = do_skeleton[s_key]
            # sub branch or stacked twigleaf
            if (isinstance(o_subtree,dict)):
                d_subtree = self.xray(o_subtree)
                do_skeleton.update({s_key : d_subtree})
            else:
                # list of values
                if (isinstance(o_subtree,list)):
                    o_value = ["value_list"]
                # value
                elif (o_subtree != None):
                    o_value = "value"
                else:
                    o_value = None
                # populate output
                do_skeleton.update({s_key : o_value})
        # output
        return(do_skeleton)


    def jsonxray_write(self):
        """
        description:
          self.xray()s the ddtree. writes the resulting skeleton
          into the self.s_ofile specified json file.
          self.s_ofile can be specified via self.set_ofile().

        input:
           - self.s_ofile: output path, filename and extension

        output:
           - ddtree.json: file

        run::

          self.set_ofile("/path/output.json")
          self.jsonxray_write()

        """
        # extract tree skeleton and write to file with standard python json library
        dd_skeleton = {}
        if (self.b_verbose != None):
            print("\nProcess jsonxray_write ...")

        # x ray
        dd_skeleton = self.xray()

        # write file with standard python json library
        if (self.b_verbose != None):
            print("Write output file: {}".format(self.s_ofile))
        with open(self.s_ofile, 'w', newline='') as f_json:
            d_json = json.dump(dd_skeleton, f_json, indent=2)
        # output
        f_json.close()
        if (self.b_verbose != None):
            print("0K")

        # output
        return(dd_skeleton)


    def parse_ddtree(self):
        """
        description:
          *internal method*,
          pareses self.s_ddtree string and
          generates lsi_branch, llsi_twig and llssbi_leaf construct.
          each tree construct need at least a branch, a twig, and a vein!
          the basic tree layouts are::

            {branch:{twig:{primary_vein: vein_value}}} and
            {branch:{twig:{primary_vein: secondary_vein: vein_value}}}

        self.s_ddtree syntax example:
          - {branch1:{branch2:{(twig1:leaf1:condense),(twig2:leaf2:condense),(twig3:leaf3:condense)}}}
          - {cellline:{plate:{(twig488:leaf488),(twig555:leaf555),(twig647:leaf647)}}}
          - this string reads out a tsv file
            with several columns specified as branch and
            separate twig label columns and leaf vein value columns.
            now when the self.s_veine, set by self.set.leaf(), is not None,
            then self.s_veine will become secondary veine, if it is None,
            then the leaf will have only a primary veine.
            the branch part can e.g. be cell line, plate, well,
            where the twig column lists the antibodies measured on one channel,
            and the leaf column list the expression value or values measured
            for this antibody at this channel.
            by default condense is set to False, vein values will not be
            condensed to one value, even when they are all the same.
            if condense is explicitly set to True, then the vein values will
            be condensed to one single value or,
            if not all values for a single vein are equal, an error is spit out.
            this is kind of the standard tsv file that comes out of
            many hts microscopes and scanners.

       self.s_ddtree syntax example:
          - {branch1:{branch2:{(twig:vein1:condense),(twig0:vein2:condense),(twig0:vein3:condense)}}}
          - {cellline:{plate:{(wavelength:value),(wavelength:s3remove),(wavelength:mean:True)}}}
          - this string reads out a tsv file
            with several columns specified as branch,
            one twig label column, several primary vein value columns,
            and maybe a seconday vein value specified by self.s_vein
            via self.set_leaf().
            such files can be generated via ddtree.tsv_write(). this kind
            of the tsv files (generates with default values) are compatible
            with pandas and R dataframes.

       self.s_ddtree syntax example:
          - {branch1:{branch2:{branch3{(vein1:vein1:condense),(vein2:vein2:condense),(vein3:vein3:condense)}}}
          - {cellline:{plate:{wavelength:{(value:value),(s3remove:s3remove),(mean:mean:True)}}}
          - this string reads out a tsv file
            with several columns specified as branch,
            the twig label is this time part of the branch.
            the primary vein label is the vein column label.
            self.s_vein set via self.set_leaf() figures as seconday vein value,
            if different from none.
            the vein values are the vein column row content.
            such files can be generated via ddtree.tsv_write(). this kind
            of the tsv files (generates with default values) are compatible
            with pandas and R dataframes.

        note:
          this syntax examples can be combined.
          have a look at the pyddtree read the docs tutorial:
          http://pyddtree.readthedocs.io/en/latest/

          if you are not heavily experienced,
          i strongly recommend to always study the resulting ddtree construct.
          especially the twig leaf level might turn out differently
          from what you have expected.
          two easy ways to analyze the construct are listed below::

            from pyddtree.ddtree import ddtree
            import json
            o_hazel = ddtree()
            o_hazel.set_ifile("/path/input.tsv")
            o_hazel.set_ddtree("{cellline:{plate:{(twig488:leaf488),(twig555:leaf555),(twig647:leaf647)}}}")
            o_hazel.tsv_read()
            o_hazelxray = o_hazel.xray()
            print(json.dumps(o_hazelxray, indent=2))

          or::

            from pyddtree.ddtree import ddtree
            o_walnut = ddtree()
            o_walnut.set_ifile("/path/input.tsv")
            o_walnut.set_ddtree("{cellline:{plate:{(twig488:leaf488),(twig555:leaf555),(twig647:leaf647)}}}")
            o_walnut.tsv_read()
            o_walnut.set_ofile("/path/output.json")
            o_walnut.jsonxray_write()

          and study the resulting file with your favorite text editor
        """
        if (self.b_verbose != None):
            print("Parse ddtree input string: {}".format(self.s_ddtree))
        # empty output
        llsi_branch = [] # [["branch1",1],["branch2",2],["branch3",3]]
        llsi_twig = []   # [["twig488",4],["twig555",5],["twig647",6]]
                         # [["twig",10]]
                         # [["twig488", 4],["twig",10]]
        llssbi_leaf = [] # [["twig488","leaf488",False,7],["twig555","leaf555",False,8],["twig647","leaf647",False,9]]
                         # [["twig","value",False,11],["twig","mean",True,12],["twig","s3remove",False,13]]
                         # [["twig488","leaf488",False,4],["twig","mean",True,12],["twig","s3remove",False,13]]

        # get branch and leaf split
        self.s_ddtree = self.s_ddtree.replace(" ","")
        s_ddtree = self.s_ddtree.replace("}","")
        ls_branch = s_ddtree.split(":{")
        s_twigleaf = ls_branch.pop(-1)
        ls_twigleaf = s_twigleaf.split(",")

        # first check
        i_total = len(ls_branch) + 1 + len(ls_twigleaf)
        if (i_total < 3):
            sys.exit("SyntaxError: self.set_ddtree string {} have at least to specify 1 branch 1 twig and 1 vein to get a working ddtree construct.".format(self.s_ddtree))

        # process ls_branch
        #print("Get branch, twig, leaf construct.")
        for s_branch in ls_branch:
            if (s_branch != ""):
                # update llsi branch
                s_branch = s_branch.replace("{","")
                lsi_branch = [s_branch,None]
                llsi_branch.append(lsi_branch)

        # process ls_twigleaf
        for s_twigleaf in ls_twigleaf:
            # extract twigleaf string
            s_twigleaf = s_twigleaf.replace("(","")
            s_twigleaf = s_twigleaf.replace(")","")
            l_twigleaf = s_twigleaf.split(":")
            b_condense = False
            if (len(l_twigleaf) > 2):
                s_condense = l_twigleaf.pop()
                if (s_condense == 'True'):
                    b_condense = True
            s_leaf = l_twigleaf.pop()
            s_twig = l_twigleaf.pop()
            # handle llsi twig
            lsi_twig = [s_twig, None]
            llsi_twig.append(lsi_twig)
            # handle llsi twig
            lssbi_leaf = [s_twig, s_leaf, b_condense, None]
            llssbi_leaf.append(lssbi_leaf)

        # output
        tl_blueprint = (llsi_branch, llsi_twig, llssbi_leaf)
        if self.b_verbose:
            print("Branch, twig, leaf construct:", tl_blueprint)
        return(tl_blueprint)


    def tsv_read(self):
        """
        description:
          read ddtree.tsv tab separated value file into ddtree object.
          the file is specified via self.s_ofile().
          how to read out the tsv file is specified via self.s_ddtree().
          a more detailed description of the s_ddtree syntax can be found at the
          self.parse_ddtree() docstring on this page.

        input:
           - self.s_ifile: input path, filename and extension
           - self.s_ddtree: ddtree syntax string.
             this string specifies how to read out the tsv file into ddtree object.

        output:
           - self

        run::

          self.set_ifile("/path/input.tsv")
          self.tsv_read()

        """
        if (self.b_verbose != None):
            print("\nProcess read_tsv ...")

        # parse s_ddtree dictionary blueprint
        tl_blueprint = self.parse_ddtree()

        # read file with standard python csv library
        if (self.b_verbose != None):
            print("Read input file: {}".format(self.s_ifile))
        if self.b_verbose:
            ls_monitor = [] # for display only
        with open(self.s_ifile, newline='') as f_csv:
            f_reader = csv.reader(f_csv, delimiter="\t")
            b_header = True
            for ls_row in f_reader:
                if (re.search("^#", ls_row[0])):
                    # handle commented out rows
                    if self.b_verbose:
                        print("Handle commented out rows.")

                elif (b_header):
                    # handle header row
                    if self.b_verbose:
                        print("Handle header row.")
                    ls_row = [n.replace(" ","") for n in ls_row]

                    # populate llsi_branch with index integers
                    llsi_branch = tl_blueprint[0]
                    for lsi_branch in llsi_branch:
                        s_index = lsi_branch[0] # branch label
                        i_index = ls_row.index(s_index)
                        lsi_branch[-1] = i_index # mutable

                    # populate llsi_twig construct
                    llsi_twig = tl_blueprint[1]
                    for lsi_twig in llsi_twig:
                        s_index = lsi_twig[0] # twig label
                        i_index = ls_row.index(s_index)
                        lsi_twig[-1] = i_index # mutable

                    # populate llssbi_leaf construct
                    llssbi_leaf = tl_blueprint[2]
                    for lssbi_leaf in llssbi_leaf:
                        s_index = lssbi_leaf[1]  # vein label
                        i_index = ls_row.index(s_index)
                        lssbi_leaf[-1] = i_index # mutable

                    # stdout
                    if self.b_verbose:
                        print("\nBranch construct:", llsi_branch)
                        print("Twig construct:", llsi_twig)
                        print("Leaf construct:", llssbi_leaf)

                    # set header flag
                    b_header = False

                else:
                    # handle data row
                    # read out data row branch
                    ls_branchreal = []
                    for lsi_branch in llsi_branch:
                        i_branch = lsi_branch[-1]
                        s_branchreal = ls_row[i_branch]
                        s_branchreal = s_branchreal.strip()
                        ls_branchreal.append(s_branchreal)

                    # read out data row twigleaf
                    ltsob_twigleaf = []
                    for lssbi_leaf in llssbi_leaf:
                        # twig part
                        s_twigtag = lssbi_leaf[0]
                        for lsi_twig in llsi_twig:
                            if (lsi_twig[0] == s_twigtag):
                                i_twig = lsi_twig[-1]
                                s_twigreal = ls_row[i_twig]
                                s_twigreal = s_twigreal.strip()
                                break
                        # leaf part
                        s_veintag = lssbi_leaf[1]
                        i_vein = lssbi_leaf[-1]
                        s_veinreal = ls_row[i_vein]
                        s_veinreal = s_veinreal.strip()
                        # handle twigvein label
                        if (s_twigtag == s_veintag):
                            # twigtag is not veintag case
                            s_twigveinlabel = s_twigtag
                        else:
                            # twigtag is veintag case
                            s_twigveinlabel = s_twigreal
                        # retype values
                        try:
                            o_veinreal = json.loads(s_veinreal)
                        except (TypeError, ValueError):
                            s_veinreal = json.dumps(s_veinreal)
                            o_veinreal = json.loads(s_veinreal)
                        # handle list values
                        if (type(o_veinreal) == list):
                            # handle none valiues
                            i_index = 0
                            for o in o_veinreal:
                                if (o in ts_none):
                                    o_veinreal[i_index] = None
                                i_index += 1
                            # transform list with < 2 elements
                            # into a single value
                            if (len(o_veinreal) == 0):
                                # none value
                                o_veinreal = None
                            elif (len(o_veinreal) == 1):
                                # single value
                                o_veinreal = o_veinreal[0]
                            elif (self.b_development) and (len(o_veinreal) > 3):
                                # for development restrict to 3 values per list
                                o_veinreal = o_veinreal[:3]
                        # handle none values
                        if (type(o_veinreal) != list):
                            if (o_veinreal in ts_none):
                                o_veinreal = None
                        # condense part
                        b_veincondense = lssbi_leaf[2]
                        # append twig leaf
                        ltsob_twigleaf.append((s_twigveinlabel, o_veinreal, b_veincondense))

                    # populate branch construct
                    # if this is the fist entry of this branch,
                    # then then branch have first to be built
                    d_popu = self
                    for s_branchreal in ls_branchreal:
                        try:
                            d_subpopu = d_popu[s_branchreal]
                        except KeyError:
                            d_subpopu = {}
                        # next iteration
                        d_popu = d_subpopu

                    # populate twig leaf construct
                    # if this is the first entry of this leaf, build new leaf,
                    # else update leaf
                    for tsob_twigleaf in ltsob_twigleaf:
                        # get real data
                        s_twigveinlabel = tsob_twigleaf[0]
                        o_veinreal = tsob_twigleaf[1]
                        b_veincondense =  tsob_twigleaf[2]

                        # populate leaf
                        try:
                            o_veintree = d_popu[s_twigveinlabel]
                            # secondary vein case
                            if (self.s_vein != None):
                                o_veintree = o_veintree[self.s_vein]

                            # twigvein exist
                            if (b_veincondense):
                                # condense
                                if (o_veinreal != o_veintree):
                                    sys.exit("Error: branch twig {} vein {} condense is set True. {} is different from {}\ninput file: {}\nparsetree syntax: {}".format(ls_branchreal, s_twigveinlabel, o_veinreal, o_veintree, self.s_ifile, self.s_ddtree))
                            elif (type(o_veinreal) != list) and (type(o_veintree) != list):
                                # value to value
                                o_veinreal = [o_veintree, o_veinreal]
                            elif (type(o_veinreal) != list) and (type(o_veintree) == list):
                                # list to value
                                o_veintree.append(o_veinreal)
                                o_veinreal = o_veintree
                            elif (type(o_veinreal) == list) and (type(o_veintree) != list):
                                # value to list
                                o_veinreal.insert(0,o_veintree)
                            else:
                                # (type(o_veinreal) == list) and (type(o_veintree) == list):
                                # list to list
                                o_veintree.extend(o_veinreal)
                                o_veinreal = o_veintree
                        except KeyError:
                            # new vein
                            pass
                        # for development restrict to 3 values per list
                        if (self.b_development) and (type(o_veinreal) == list) and (len(o_veinreal) > 3):
                            o_veinreal = o_veinreal[:3]
                        # populate leaf with new or updated vein
                        if (self.s_vein == None):
                            # primary vein case
                            d_popu.update({s_twigveinlabel:o_veinreal})
                        else:
                            # secondary vein case
                            d_popu.update({s_twigveinlabel:{self.s_vein:o_veinreal}})

                        # update output dictionary
                        self.d_leaf = d_popu
                        self.ls_branch = ls_branchreal
                        self.update_branch()  # self.d_leaf=d_popu, ls_branch=ls_branchreal

                    # for display only
                    if self.b_verbose:
                        if (ls_monitor != ls_branchreal):
                            ls_monitor = ls_branchreal
                            print("Handle ddtree branch: {}".format(ls_branchreal))
            # output
            f_csv.close()
            if (self.b_verbose != None):
                print("0K")


    def tsv_write(self, b_listing=True, ls_veinpop=None):
        """
        description:
          write self.s_ofile ddtree.tsv tab separated value files
          via standard python csv library.

        input:
           - b_listing: to specify how to print leaf lists.
             True outputs list as pandas and R dataframe compatible listing,
             False outputs lists inline, similar to json file output.
             default is True.
           - ls_veinpop: None. list of vein which not should be outputed
             to the tsv file. None will output any leaf vein.
           - self.s_ofile: output path, filename and extension
           - self.s_bark: set via self.set_bark() to explicitly specify
             the branch header part or
           - self.s_ddtree: set via self.set_ddtree() to get
             the branch header part extracted.
             if both, self.s_bark and self.s_ddtree, are not None,
             then then self.s_bark will have priority.
             if self.s_bark and self.s_ddtree are both None, then
             branch labeling will result in "branch0","branch1",...

        output:
           - ddtree.tsv: file in listing or inline version

        run::

          self.set_ofile("/path/output.json")
          self.tsv_write()
          self.tsv_write(b_listing=False)

        """
        if (self.b_verbose != None):
            print("\nProcess tsv_write ...")

        # check if leaf0k
        self.leaf0k()

        # set header flag
        self.b_header = False

        # write lines
        d_calxargs = {}
        d_calxargs.update({"ls_veinpop": ls_veinpop})
        if b_listing:
            self.calx(o_calx=tsv_writeleaflisting, d_calxargs=d_calxargs)
        else:
            self.calx(o_calx=tsv_writeleafinline, d_calxargs=d_calxargs)
