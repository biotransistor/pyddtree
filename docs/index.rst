.. ddtree documentation master file, created by
   sphinx-quickstart on Sat Apr 16 11:58:34 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

python3 Dictionary Ditctionary Tree documentation
=================================================
.. here goes description, evt imported from source code
.. at gitlab do README.rst file with version log!, author list, gpl3 license. readme.rst links to readthedocs.org

Abstract:

ddtree provides an easy syntax to read nearly any tsv tab separated value file
and ddtree json files into a python dictionary of dictionary construct.
ddtree owns some further commands to write ddtree objects into json files or
panda dataframe, R dataframe and excel compatible tsv flat files.
namely this commands are: tsv_read, tsv_write, json_read, json_write and jsonxray_write.

in addition has ddtree a simple set of commands to manipulate the tree structure.
those are: get, put, rename, delete, twigpop, leaf0k and xray.
basic command attributes are consequently set by set commands
(set_ifile, set_ofile, set_png, set_ddtree, set_bark, set_leaf, set_verbose and set_developer).

for freaks who prefer trees over dataframes provides the calx library
additionally a growing number of mathematic and plot functions
which work directly and automatically on the leaf layer of the ddtree construct.
however, the aim is not to be better then pandas and seaborn.
the aim is to be complementary.
it turns out that ddtree is quit magnificent to write bioinformatics analysis pipelines.


Contents:

.. toctree::
   :maxdepth: 2

   instal
   tutorial
   man_ddtree
   man_calx
   fdl-1_3

Source code:
https://gitlab.com/biotransistor/pyddtree

Contact:
send bug reports, vegetarian pizzas and love letters to ulmusfagus at zoho dot com.



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
