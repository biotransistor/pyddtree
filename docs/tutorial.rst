Dictionary of Dictionary Tree tutorial
======================================

This example input.tsv is a heavily shortened version of a tab separated value
flat file you might get form your fellow wet lab scientist, from a
immuno histo chemistry experiment, where  in *HEK293* cells
*DAPI (4',6-diamidino-2-phenylindole)* intensity at *395nm* and
*INS (inuline)* intensity at *512nm* wavelength was measured.
This shortened file has data from one wellplate *(20160506)* on two wells *(A01, A02)*
and can be found in the source code package.

Tab separated input (/path/input.tsv):

======== ==== ======== ====== ===== ========== ============== ============== =========
Plate    Well CellLine Object 395nm 512nm      Intensity395nm Intensity512nm CellCount
======== ==== ======== ====== ===== ========== ============== ============== =========
20160506 A01  HEK293   1      DAPI  INS_P01308 331.308079     74.197754      1234
20160506 A01  HEK293   2      DAPI  INS_P01308 709.588133     254.110180     1234
20160506 A01  HEK293   3      DAPI  INS_P01308 635.814822     68.511627      1234
20160506 A02  HEK293   1      DAPI  INS_P01308 383.754444     101.102400     1337
20160506 A02  HEK293   2      DAPI  INS_P01308 483.976757     98.300056      1337
======== ==== ======== ====== ===== ========== ============== ============== =========

Let's load some python3 libraries used to analyze and manipulate this data with ddtree::

  from pyddtree.ddtree import ddtree  # ddtree main library
  from pyddtree import calx # ddtree compatible mathematical function library
  import json  # to display ddtree object

Now we will load the input.tsv and have a look at the resulting ddtree structure.
Try to understand the syntax in the string inside the *birch.set_ddtree()* brackets.
It is not that difficult:

**"{Plate:{Well:{(395nm:Intensity395nm),(512nm:Intensity512nm),(CellCount:CellCount:True),(CellLine:CellLine:True)}}}"**

Study the result with *o_birch*, *print(o_birch)* and *print(json.dumps(o_birch, indent=2))* command
and with the json editor: http://jsoneditoronline.org .
Because this is a heavily shortened table, this works well.
However, when you work with a table with hundreds or tousends of data rows,
you will start to understand the usefulness of the *o_birch.xray()* function to analyze the resulting ddtree structure.

Code::

  # set input file
  o_birch = ddtree()
  o_birch.set_ifile("input.tsv")
  # load input file: {branch:{twig:{primary_vein: vein_value}}}
  o_birch.set_ddtree("{Plate:{Well:{(395nm:Intensity395nm),(512nm:Intensity512nm),(CellCount:CellCount:True),(CellLine:CellLine:True)}}}")
  o_birch.tsv_read()
  o_birch
  print(o_birch)
  print(json.dumps(o_birch, indent=2))

  # set input file
  o_birch = ddtree()
  # load input file: {branch:{twig:{primary_vein: secondary_vein: vein_value}}}
  o_birch.set_ddtree("{Plate:{Well:{(395nm:Intensity395nm),(512nm:Intensity512nm),(CellCount:CellCount:True),(CellLine:CellLine:True)}}}")
  o_birch.set_leaf("value")  # secondary vein
  o_birch.tsv_read()
  o_birch
  print(o_birch)
  print(json.dumps(o_birch, indent=2))

  # xray ddtree construct
  o_birch.xray()
  print(o_birch.xray())
  print(json.dumps(o_birch.xray(), indent=2))


Lets have a look at the *o_birch.get()* function and manipulate the tree a bit by
introducing an additional leaf vein called foo via *o_birch.put()* function.
and delet the vein later again with the *o_birch.delete()* function.
The *o_birch.leaf0k()* function checks for consistent leaf structure along the leaf layer.
Notice that *o_birch.leaf0k()* throws an error, when we only manipulated one
and not all of the leafs on the layer.

Code::

  # get CellLine vein values from any leaf
  o_birch.set_leaf("CellLine")
  o_birch.get()
  print(o_birch.l_get)

  # put foo vein only on leaf A01
  o_birch.set_leaf("CellLine")
  o_birch.put(s_vein="foo", o_value="figthers", ls_branchtwigleaf=["20160506","A01"])
  o_birch.leaf0k()
  print(json.dumps(o_birch, indent=2))

  # put foo vein on any leaf of the set_leaf specified leaf layers.
  o_birch.set_leaf("CellLine")
  o_birch.put(s_vein="foo", o_value="fighters")
  o_birch.leaf0k()
  print(json.dumps(o_birch, indent=2))

  # delete foo vein only on leaf A01
  o_birch.set_leaf("foo")
  o_birch.delete(["20160506","A01"])
  o_birch.leaf0k()
  print(json.dumps(o_birch, indent=2))

  # delete foo vein on any leaf.
  o_birch.set_leaf("foo")
  o_birch.delete()
  o_birch.leaf0k()
  print(json.dumps(o_birch, indent=2))


Let's get the *calx.mean* function on the leaf layer on the
intensity measurement working and have a look at the result.

Code::

  # calculate dapi mean
  o_birch.set_leaf("value")
  o_birch.calx(calx.mean)
  o_birch.set_leaf("mean")
  o_birch.rename("mean_value")
  print(json.dumps(o_birch, indent=2))


Let's pop the value vein.

Code::

  # twigpop
  o_birch.set_leaf("DAPI")
  o_birch.twigpop("value")
  print(json.dumps(o_birch, indent=2))

  # twigpop
  o_birch.set_leaf("INS_P01308")
  o_birch.twigpop("value")
  print(json.dumps(o_birch, indent=2))


And finally, print out the ddtree object in all possible tsv tab separated value
and json file formats.

Code::

  # write /path/output_listing.tsv file
  o_birch.set_leaf("CellLine")
  o_birch.set_ofile("output_listing.tsv")
  o_birch.tsv_write()

  # write /path/output_inline.tsv file
  o_birch.set_leaf("CellLine")
  o_birch.set_ofile("output_inline.tsv")
  o_birch.tsv_write(b_listing=False)

  # write /path/output.json file
  o_birch.set_ofile("output.json")
  o_birch.json_write()

  # write /path/output_xray.json file
  o_birch.set_ofile("output_xray.json")
  o_birch.jsonxray_write()


Tab separated listed output (/path/output_listing.tsv):

======== ==== ========= ======== ========== ==========
Plate    Well CellCount CellLine DAPI       INS_P01308
======== ==== ========= ======== ========== ==========
20160506 A02  1337      HEK293   383.754444 101.102400
20160506 A02  1337      HEK293   483.976757 98.300056
20160506 A01  1234      HEK293   331.308079 74.197754
20160506 A01  1234      HEK293   709.588133 254.110180
20160506 A01  1234      HEK293   635.814822 68.5116277
======== ==== ========= ======== ========== ==========

Tab separated inline output (/path/output_inline.tsv):

======== ==== ========= ======== =========================== =========================
Plate    Well CellCount CellLine DAPI                                 INS_P01308
======== ==== ========= ======== =========================== =========================
20160506 A02  1337      "HEK293" [383.754, 483.976]          [101.102, 98.300]
20160506 A01  1234      "HEK293" [331.308, 709.588, 635.814] [74.197, 254.110, 68.511]
======== ==== ========= ======== =========================== =========================

Json output (/path/output.json and /path/output_xray.json)::

  {
    "20160506": {
      "A01": {
        "CellCount": 1234,
        "DAPI": [
          331.308079,
          709.588133,
          635.814822
        ],
        "CellLine": "HEK293",
        "INS_P01308": [
          74.197754,
          254.11018,
          68.5116277
        ]
      },
      "A02": {
        "CellCount": 1337,
        "DAPI": [
          383.754444,
          483.976757
        ],
        "CellLine": "HEK293",
        "INS_P01308": [
          101.1024,
          98.300056
        ]
      }
    }
  }

That's all, folks!
May the force be with you.
