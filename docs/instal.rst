Dictionary of Dictionary Tree instal
====================================

Requirements: **python3**.

for a simple instal this should do the trick (*this is not yet working!*)::

  pip3 install pyddtree


alternativle:

 #. *git clone* or *git fork and clone* the project.
 #. after cloning execute the unity test routine.
 #. don't forget to take care that pyddtree is somehow in your PYTHONPATH.

::

  git clone https://gitlab.com/biotransistor/pyddtree.git
  cd pyddtree
  python3 test_ddtree.py
