import unittest

# load libraries
import copy
import json
from pyddtree.ddtree import ddtree

#def test_ddtree_calc(self):
#def test_ddtree_plot(self):

class TestDdtreeInit(unittest.TestCase):

    #def setUp(self):
    #def tearDown(self):

    def test_ddtree_init(self):
        """ test init and set_* methodes """
        o_sequoia = ddtree()
        self.assertEqual(o_sequoia, {})
        self.assertEqual(o_sequoia.ls_bark, None)
        self.assertEqual(o_sequoia.s_ddtree, None)
        self.assertEqual(o_sequoia.b_development, False)
        self.assertEqual(o_sequoia.s_ifile, None)
        self.assertEqual(o_sequoia.s_vein, None)
        self.assertEqual(o_sequoia.s_ofile, None)
        self.assertEqual(o_sequoia.b_png, False)
        self.assertEqual(o_sequoia.b_verbose, False)


class TestDdtreeRead(unittest.TestCase):
    def test_ddtree_jsonread(self):
        # result
        dd_pine = {
            'cellline_a': {
                'run_a': {'well_a': {
                    'nm_360': {'value': [54, 63, 72]},
                    'nm_240': {'value': [27, 36, 45]},
                    'nm_120': {'value': [0, 9, 18]}}},
                'run_b': {'well_a': {
                    'nm_360': {'value': [57, 66, 75]},
                    'nm_240': {'value': [30, 39, 48]},
                    'nm_120': {'value': [3, 12, 21]}}},
                'run_c': {'well_a': {
                    'nm_360': {'value': [60, 69, 78]},
                    'nm_240': {'value': [33, 42, 51]},
                    'nm_120': {'value': [6, 15, 24]}}}},
            'cellline_b': {
                'run_a': {'well_a': {
                    'nm_360': {'value': [55, 64, 73]},
                    'nm_240': {'value': [28, 37, 46]},
                    'nm_120': {'value': [1, 10, 19]}}},
                'run_b': {'well_a': {
                    'nm_360': {'value': [58, 67, 76]},
                    'nm_240': {'value': [31, 40, 49]},
                    'nm_120': {'value': [4, 13, 22]}}},
                'run_c': {'well_a': {
                    'nm_360': {'value': [61, 70, 79]},
                    'nm_240': {'value': [34, 43, 52]},
                    'nm_120': {'value': [7, 16, 25]}}}},
            'cellline_c': {
                'run_a': {'well_a': {
                    'nm_360': {'value': [56, 65, 74]},
                    'nm_240': {'value': [29, 38, 47]},
                    'nm_120': {'value': [2, 11, 20]}}},
                'run_b': {'well_a': {
                    'nm_360': {'value': [59, 68, 77]},
                    'nm_240': {'value': [32, 41, 50]},
                    'nm_120': {'value': [5, 14, 23]}}},
                'run_c': {'well_a': {
                    'nm_360': {'value': [62, 71, 80]},
                    'nm_240': {'value': [35, 44, 53]},
                    'nm_120': {'value': [8, 17, 26]}}}}
        }

        # read json
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        self.assertEqual(o_sequoia, dd_pine)


    def test_ddtree_tsvmicroscoperead(self):
        """
        s_ddtree syntax example:
        {branch1:{branch2:{(twig1:leaf10,(twig2:leaf2),(twig3:leaf3)}}}
        """
        # result
        dd_pine = {
            'cellline_c': {
                'run_c': {
                    'well_a': {
                        'nm_360': {'value': [62, 71, 80]},
                        'nm_240': {'value': [35, 44, 53]},
                        'nm_120': {'value': [8, 17, 26]}}},
                'run_a': {
                    'well_a': {
                        'nm_360': {'value': [56, 65, 74]},
                        'nm_240': {'value': [29, 38, 47]},
                        'nm_120': {'value': [2, 11, 20]}}},
                'run_b': {'well_a': {
                    'nm_360': {'value': [59, 68, 77]},
                    'nm_240': {'value': [32, 41, 50]},
                    'nm_120': {'value': [5, 14, 23]}}}},
            'cellline_b': {
                'run_c': {
                    'well_a': {
                        'nm_360': {'value': [61, 70, 79]},
                        'nm_240': {'value': [34, 43, 52]},
                        'nm_120': {'value': [7, 16, 25]}}},
                'run_a': {
                    'well_a': {
                        'nm_360': {'value': [55, 64, 73]},
                        'nm_240': {'value': [28, 37, 46]},
                        'nm_120': {'value': [1, 10, 19]}}},
                'run_b': {
                    'well_a': {
                        'nm_360': {'value': [58, 67, 76]},
                        'nm_240': {'value': [31, 40, 49]},
                        'nm_120': {'value': [4, 13, 22]}}}},
            'cellline_a': {
                'run_c': {
                    'well_a': {
                        'nm_360': {'value': [60, 69, 78]},
                        'nm_240': {'value': [33, 42, 51]},
                        'nm_120': {'value': [6, 15, 24]}}},
                'run_a': {
                    'well_a': {
                        'nm_360': {'value': [54, 63, 72]},
                        'nm_240': {'value': [27, 36, 45]},
                        'nm_120': {'value': [0, 9, 18]}}},
                'run_b': {
                    'well_a': {
                        'nm_360': {'value': [57, 66, 75]},
                        'nm_240': {'value': [30, 39, 48]},
                        'nm_120': {'value': [3, 12, 21]}}}}}

        # read tsv type microscope
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_leaf("value")
        o_sequoia.set_ifile("ddtree_smoketest_microscope01.tsv")
        o_sequoia.set_ddtree("{branch1:{branch2:{branch3:{(twig1:leaf1),(twig2:leaf2),(twig3:leaf3)}}}}")
        o_sequoia.tsv_read()
        self.assertEqual(o_sequoia, dd_pine)


    def test_ddtree_tsvinlineread(self):
        """
        s_ddtree syntax example:
        {branch1:{branch2:{branch3:{(wavelength:value)}}}}")
        """
        # result
        dd_pine = {
            'cellline_a': {
                'run_c': {
                    'well_a': {
                        'nm_360': [60, 69],
                        'nm_240': 33,
                        'nm_120': 6}},
                'run_b': {
                    'well_a': {
                        'nm_360': [57, 66, 75],
                        'nm_240': [30, 39, 48],
                        'nm_120': [3, 12, 21]}},
                'run_a': {
                    'well_a': {
                        'nm_360': [54, 63, 72],
                        'nm_240': [27, 36, 45],
                        'nm_120': [0, 9, 18]}}},
            'cellline_b': {
                'run_c': {
                    'well_a': {
                        'nm_360': [61, 70, 79],
                        'nm_240': [34, 43, 52],
                        'nm_120': [7, 16, 25]}},
                'run_b': {'well_a': {
                    'nm_360': [58, 67, 76],
                    'nm_240': [31, 40, 49],
                    'nm_120': [4, 13, 22]}},
                'run_a': {
                    'well_a': {
                        'nm_360': [55, None, None],
                        'nm_240': [28, 37, 46],
                        'nm_120': [1, 10, 19]}}},
            'cellline_c': {
                'run_c': {
                    'well_a': {
                        'nm_360': [62, 71, 80],
                        'nm_240': [35, 44, 53],
                        'nm_120': [8, 17, 26]}},
                'run_b': {
                    'well_a': {
                        'nm_360': [59, 68, 77],
                        'nm_240': [32, 41, 50],
                        'nm_120': [5, 14, 23]}},
                'run_a': {
                    'well_a': {
                        'nm_360': [56, 65, 74],
                        'nm_240': [29, 38, 47],
                        'nm_120': [2, 11, 20]}}}}

        # read tsv type inline
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest_inline01.tsv")
        o_sequoia.set_ddtree("{branch1:{branch2:{branch3:{(wavelength:value)}}}}")
        o_sequoia.tsv_read()
        self.assertEqual(o_sequoia, dd_pine)


    def test_ddtree_tsvlistingread(self):
        """
        s_ddtree syntax example:
        {branch1:{branch2:{branch3:{wavelength:{(value:value),(someremoved:someremoved:False),(mean:mean:True),(stainset:stainset:True),(stackedleaf:stackedleaf:True)}}}}
        """
        # result
        dd_pine = {
            'cellline_a': {
                'run_c': {'well_a': {
                    'nm_120': {
                        'someremoved': [None, 15, 24],
                        'stainset': 'theset',
                        'value': [6, 15, 24],
                        'mean': 15,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [33, 42, 51],
                        'stainset': 'theset',
                        'value': [33, 42, 51],
                        'mean': 42,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [None, 69, None],
                        'stainset': 'theset',
                        'value': [60, 69, 78],
                        'mean': 69,
                        'stackedleaf': '{leaf}'}}},
                'run_b': {'well_a': {
                    'nm_120': {
                        'someremoved': [3, 12, None],
                        'stainset': 'theset',
                        'value': [3, 12, 21],
                        'mean': 12,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [30, 39, 48],
                        'stainset': 'theset',
                        'value': [30, 39, 48],
                        'mean': 39,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [57, None, None],
                        'stainset': 'theset',
                        'value': [57, 66, 75],
                        'mean': 66,
                        'stackedleaf': '{leaf}'}}},
                'run_a': {'well_a': {
                    'nm_120': {
                        'someremoved': [None, 9, 18],
                        'stainset': 'theset',
                        'value': [0, 9, 18],
                        'mean': 9,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [27, 36, 45],
                        'stainset': 'theset',
                        'value': [27, 36, 45],
                        'mean': 36,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [None, None, 72],
                        'stainset': 'theset',
                        'value': [54, 63, 72],
                        'mean': 63,
                        'stackedleaf': '{leaf}'}}}},
            'cellline_b': {
                'run_c': {'well_a': {
                    'nm_120': {
                        'someremoved': [7, 16, None],
                        'stainset': 'theset',
                        'value': [7, 16, 25],
                        'mean': 16,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [34, 43, None],
                        'stainset': 'theset',
                        'value': [34, 43, 52],
                        'mean': 43,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [61, 70, None],
                        'stainset': 'theset',
                        'value': [61, 70, 79],
                        'mean': 70,
                        'stackedleaf': '{leaf}'}}},
                'run_b': {'well_a': {
                    'nm_120': {
                        'someremoved': [4, 13, None],
                        'stainset': 'theset',
                        'value': [4, 13, 22],
                        'mean': 13, 'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [31, 40, None],
                        'stainset':
                        'theset',
                        'value': [31, 40, 49],
                        'mean': 40,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [58, 67, None],
                        'stainset': 'theset',
                        'value': [58, 67, 76],
                        'mean': 67, 'stackedleaf': '{leaf}'}}},
                'run_a': {'well_a': {
                    'nm_120': {
                        'someremoved': [1, 10, None],
                        'stainset': 'theset',
                        'value': [1, 10, 19],
                        'mean': 10,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [28, 37, None],
                        'stainset':'theset',
                        'value': [28, 37, 46],
                        'mean': 37,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [55, 64, None],
                        'stainset': 'theset',
                        'value': [55, 64, 73],
                        'mean': 64,
                        'stackedleaf': '{leaf}'}}}},
            'cellline_c': {
                'run_c': {'well_a': {
                    'nm_120': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset',
                        'value': [8, 17, 26],
                        'mean': 17,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset',
                        'value': [35, 44, 53],
                        'mean': 44,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset',
                        'value': [62, 71, 80],
                        'mean': 71,
                        'stackedleaf': '{leaf}'}}},
                'run_b': {'well_a': {
                    'nm_120': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset', 'value': [5, 14, 23],
                        'mean': 14, 'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset', 'value': [32, 41, 50],
                        'mean': 41, 'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset', 'value': [59, 68, 77],
                        'mean': 68,
                        'stackedleaf': '{leaf}'}}},
                'run_a': {'well_a': {
                    'nm_120': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset',
                        'value': [2, 11, 20],
                        'mean': 11,
                        'stackedleaf': '{leaf}'},
                    'nm_240': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset',
                        'value': [29, 38, 47],
                        'mean': 38,
                        'stackedleaf': '{leaf}'},
                    'nm_360': {
                        'someremoved': [None, None, None],
                        'stainset': 'theset',
                        'value': [56, 65, 74],
                        'mean': 65,
                        'stackedleaf': '{leaf}'}}}}}

        # read tsv type listing
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest_listing01.tsv")
        o_sequoia.set_ddtree("{branch1:{branch2:{branch3:{wavelength:{(value:value),(someremoved:someremoved:False),(mean:mean:True),(stainset:stainset:True),(stackedleaf:stackedleaf:True)}}}}")
        o_sequoia.tsv_read()
        self.assertEqual(o_sequoia, dd_pine)


    def test_ddtree_tsvdataframeread(self):
        """
        s_ddtree syntax example:
        {branch1:{branch2:{branch3:{(nm_120:nm_120),(nm_240:nm_240),(nm_360:nm_360),(stainset:stainset:True),(entropy:entropy:True)}}}}
        """
        # result
        dd_pine = {
            'cellline_a': {
                'run_c': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [6, 15, 24],
                    'nm_240': [33, 42, 51],
                    'nm_360': [60, 69, 78]}},
                'run_b': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [3, 12, 21],
                    'nm_240': [30, 39, 48],
                    'nm_360': [57, 66, 75]}},
                'run_a': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [0, 9, 18],
                    'nm_240': [27, 36, 45],
                    'nm_360': [54, 63, 72]}}},
            'cellline_c': {
                'run_c': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [8, 17, 26],
                    'nm_240': [35, 44, 53],
                    'nm_360': [62, 71, 80]}},
                'run_b': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [5, 14, 23],
                    'nm_240': [32, 41, 50],
                    'nm_360': [59, 68, 77]}},
                'run_a': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [2, 11, 20],
                    'nm_240': [29, 38, 47],
                    'nm_360': [56, 65, 74]}}},
            'cellline_b': {
                'run_c': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [7, 16, 25],
                    'nm_240': [34, 43, 52],
                    'nm_360': [61, 70, 79]}},
                'run_b': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [4, 13, 22],
                    'nm_240': [31, 40, 49],
                    'nm_360': [58, 67, 76]}},
                'run_a': {'well_a': {
                    'entropy': 3.1415,
                    'stainset': 'theset',
                    'nm_120': [1, 10, 19],
                    'nm_240': [28, 37, 46],
                    'nm_360': [55, 64, 73]}}}}

        # read tsv type dataframe
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest_listing02.tsv")
        o_sequoia.set_ddtree("{branch1:{branch2:{branch3:{(nm_120:nm_120),(nm_240:nm_240),(nm_360:nm_360),(stainset:stainset:True),(entropy:entropy:True)}}}}")
        o_sequoia.tsv_read()
        self.assertEqual(o_sequoia, dd_pine)


class TestDdtreeWrite(unittest.TestCase):
    #def setUp(self):

    def test_ddtree_jsonwrite(self):
        # sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        o_sequoia.set_ofile("smoketest_jsonwrite_output.json")
        o_sequoia.json_write()
        # maple
        o_maple = ddtree()
        o_maple.b_verbose = None
        o_maple.set_ifile("smoketest_jsonwrite_output.json")
        o_maple.json_read()
        self.assertEqual(o_sequoia, o_maple)


    def test_ddtree_jsonxraywrite(self):
        # maple
        dd_maple = {
            "cellline_a": {
                "run_c": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}},
                "run_b": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}},
                "run_a": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}}},
            "cellline_b": {
                "run_c": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}},
                "run_b": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}},
                "run_a": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}}},
            "cellline_c": {
                "run_c": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}},
                "run_b": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}},
                "run_a": {
                    "well_a": {
                        "nm_240": ["value_list"],
                        "entropy": "value",
                        "stainset": "value",
                        "nm_120": ["value_list"],
                        "nm_360": ["value_list"]}}}}

        # seqiuoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest01.json")
        o_sequoia.json_read()
        o_sequoia.set_ofile("smoketest_jsonxraywrite_output.json")
        o_sequoiaclone = copy.deepcopy(o_sequoia)
        dd_sequoiaxray = o_sequoia.jsonxray_write()
        self.assertEqual(dd_sequoiaxray, dd_maple)
        self.assertEqual(o_sequoia, o_sequoiaclone)


    def test_ddtree_tsvlistingwriter(self):
        # load sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest02.json")
        o_sequoia.json_read()
        # tsv listing write
        o_sequoia.set_leaf("entropy")
        o_sequoia.set_ofile("smoketest_tsvlistingwrite_output.tsv")
        o_sequoia.tsv_write(b_listing=True)
        # load tsv listing
        o_maple = ddtree()
        o_maple.b_verbose = None
        o_maple.set_ddtree("{branch0:{branch1:{branch2:{(entropy:entropy:True),(nm_120:nm_120),(nm_240:nm_240),(nm_360:nm_360),(stainset:stainset:True)}}}")
        o_maple.set_ifile("smoketest_tsvlistingwrite_output.tsv")
        o_maple.tsv_read()
        # compare
        self.assertEqual(o_sequoia, o_maple)


    def test_ddtree_tsvinlinewrite(self):
        # load sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest02.json")
        o_sequoia.json_read()
        # tsv inline write
        o_sequoia.set_leaf("entropy")
        o_sequoia.set_bark(["stump0","stump1","stump2"])  #"stump3"
        o_sequoia.set_ofile("smoketest_tsvinlinewrite_output.tsv")
        o_sequoia.tsv_write(b_listing=False)
        # maple
        o_maple = ddtree()
        o_maple.b_verbose = None
        o_maple.set_ddtree("{stump0:{stump1:{stump2:{(entropy:entropy:True),(nm_120:nm_120),(nm_240:nm_240),(nm_360:nm_360),(stainset:stainset:True)}}}")
        o_maple.set_ifile("smoketest_tsvinlinewrite_output.tsv")
        o_maple.tsv_read()
        # compare
        self.assertEqual(o_sequoia, o_maple)


class TestDdtreeManipu(unittest.TestCase):
    def test_ddtree_leaf0k(self):
        # sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        o_sequoia.set_leaf("value")
        # cause error
        #s_vein = "mutation"
        #o_value = "break"
        #ls_branchtwigleaf=["cellline_a","run_a","well_a","nm_120"]
        #o_sequoia.put(s_vein=s_vein, o_value=o_value, ls_branchtwigleaf=ls_branchtwigleaf)
        # leaf0k
        o_sequoia.leaf0k()
        self.assertEqual(o_sequoia.es_vein, {'value'})


    def test_ddtree_get(self):
        # seqoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        # get all
        o_sequoia.set_leaf("value")
        o_sequoia.get()
        i_get = sum(o_sequoia.l_get)
        self.assertEqual(i_get, 3240)
        # get branch twig leaf
        ls_branchtwigleaf=["cellline_a","run_a","well_a","nm_120"]
        o_sequoia.get(ls_branchtwigleaf=ls_branchtwigleaf)
        i_get = sum(o_sequoia.l_get)
        self.assertEqual(i_get, 27)


    def test_ddtree_delete(self):
        # result
        dd_birch = {
            'cellline_b': {
                'run_b': {'well_a': {
                    'nm_120': {'value': [4, 13, 22]},
                    'nm_360': {'value': [58, 67, 76]},
                    'nm_240': {'value': [31, 40, 49]}}},
                'run_c': {'well_a': {
                    'nm_120': {'value': [7, 16, 25]},
                    'nm_360': {'value': [61, 70, 79]},
                    'nm_240': {'value': [34, 43, 52]}}},
                'run_a': {'well_a': {
                    'nm_120': {'value': [1, 10, 19]},
                    'nm_360': {'value': [55, 64, 73]},
                    'nm_240': {'value': [28, 37, 46]}}}},
            'cellline_c': {
                'run_b': {'well_a': {
                    'nm_120': {'value': [5, 14, 23]},
                    'nm_360': {'value': [59, 68, 77]},
                    'nm_240': {'value': [32, 41, 50]}}},
                'run_c': {'well_a': {
                    'nm_120': {'value': [8, 17, 26]},
                    'nm_360': {'value': [62, 71, 80]},
                    'nm_240': {'value': [35, 44, 53]}}},
                'run_a': {'well_a': {
                    'nm_120': {'value': [2, 11, 20]},
                    'nm_360': {'value': [56, 65, 74]},
                    'nm_240': {'value': [29, 38, 47]}}}},
            'cellline_a': {
                'run_b': {'well_a': {
                    'nm_120': {'value': [3, 12, 21]},
                    'nm_360': {'value': [57, 66, 75]},
                    'nm_240': {'value': [30, 39, 48]}}},
                'run_c': {'well_a': {
                    'nm_120': {'value': [6, 15, 24]},
                    'nm_360': {'value': [60, 69, 78]},
                    'nm_240': {'value': [33, 42, 51]}}},
                'run_a': {'well_a': {
                    'nm_120': {},
                    'nm_360': {},
                    'nm_240': {}}}}}

        # load sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        # delete branch twig leaf vein
        o_sequoia.set_leaf("value")
        ls_branchtwigleaf=["cellline_a","run_a","well_a"]
        o_sequoia.delete(ls_branchtwigleaf=ls_branchtwigleaf)
        self.assertEqual(o_sequoia, dd_birch)


    def test_ddtree_put(self):
        # result
        dd_beech = {
            'cellline_a': {
                'run_a': {'well_a': {
                    'nm_240': {'value': [27, 36, 45], 'smoke': True},
                    'nm_120': {'value': [0, 9, 18], 'smoke': True},
                    'nm_360': {'value': [54, 63, 72], 'smoke': True}}},
                'run_b': {'well_a': {
                    'nm_240': {'value': [30, 39, 48], 'smoke': True},
                    'nm_120': {'value': [3, 12, 21], 'smoke': True},
                    'nm_360': {'value': [57, 66, 75], 'smoke': True}}},
                'run_c': {'well_a': {
                    'nm_240': {'value': [33, 42, 51], 'smoke': True},
                    'nm_120': {'value': [6, 15, 24], 'smoke': True},
                    'nm_360': {'value': [60, 69, 78], 'smoke': True}}}},
            'cellline_b': {
                'run_a': {'well_a': {
                    'nm_240': {'value': [28, 37, 46], 'smoke': True},
                    'nm_120': {'value': [1, 10, 19], 'smoke': True},
                    'nm_360': {'value': [55, 64, 73], 'smoke': True}}},
                'run_b': {'well_a': {
                    'nm_240': {'value': [31, 40, 49], 'smoke': True},
                    'nm_120': {'value': [4, 13, 22], 'smoke': True},
                    'nm_360': {'value': [58, 67, 76], 'smoke': True}}},
                'run_c': {'well_a': {
                    'nm_240': {'value': [34, 43, 52], 'smoke': True},
                    'nm_120': {'value': [7, 16, 25], 'smoke': True},
                    'nm_360': {'value': [61, 70, 79], 'smoke': True}}}},
            'cellline_c': {
                'run_a': {'well_a': {
                    'nm_240': {'value': [29, 38, 47], 'smoke': True},
                    'nm_120': {'value': [2, 11, 20], 'smoke': True},
                    'nm_360': {'value': [56, 65, 74], 'smoke': True}}},
                'run_b': {'well_a': {
                    'nm_240': {'value': [32, 41, 50], 'smoke': True},
                    'nm_120': {'value': [5, 14, 23], 'smoke': True},
                    'nm_360': {'value': [59, 68, 77], 'smoke': True}}},
                'run_c': {'well_a': {
                    'nm_240': {'value': [35, 44, 53], 'smoke': True},
                    'nm_120': {'value': [8, 17, 26], 'smoke': True},
                    'nm_360': {'value': [62, 71, 80], 'smoke': True}}}}}

        dd_birch = {
            'cellline_b': {
                'run_b': {'well_a': {
                    'nm_120': {'value': [4, 13, 22]},
                    'nm_360': {'value': [58, 67, 76]},
                    'nm_240': {'value': [31, 40, 49]}}},
                'run_c': {'well_a': {
                    'nm_120': {'value': [7, 16, 25]},
                    'nm_360': {'value': [61, 70, 79]},
                    'nm_240': {'value': [34, 43, 52]}}},
                'run_a': {'well_a': {
                    'nm_120': {'value': [1, 10, 19]},
                    'nm_360': {'value': [55, 64, 73]},
                    'nm_240': {'value': [28, 37, 46]}}}},
            'cellline_c': {
                'run_b': {'well_a': {
                    'nm_120': {'value': [5, 14, 23]},
                    'nm_360': {'value': [59, 68, 77]},
                    'nm_240': {'value': [32, 41, 50]}}},
                'run_c': {'well_a': {
                    'nm_120': {'value': [8, 17, 26]},
                    'nm_360': {'value': [62, 71, 80]},
                    'nm_240': {'value': [35, 44, 53]}}},
                'run_a': {'well_a': {
                    'nm_120': {'value': [2, 11, 20]},
                    'nm_360': {'value': [56, 65, 74]},
                    'nm_240': {'value': [29, 38, 47]}}}},
            'cellline_a': {
                'run_b': {'well_a': {
                    'nm_120': {'value': [3, 12, 21]},
                    'nm_360': {'value': [57, 66, 75]},
                    'nm_240': {'value': [30, 39, 48]}}},
                'run_c': {'well_a': {
                    'nm_120': {'value': [6, 15, 24]},
                    'nm_360': {'value': [60, 69, 78]},
                    'nm_240': {'value': [33, 42, 51]}}},
                'run_a': {'well_a': {
                    'nm_120': {'value': 'smoke'},
                    'nm_360': {'value': [54, 63, 72]},
                    'nm_240': {'value': [27, 36, 45]}}}}}

        # load sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        # put all
        o_sequoia.set_leaf("value")
        s_vein = "smoke"
        o_value = True
        o_sequoia.put(s_vein=s_vein, o_value=o_value)
        self.assertEqual(o_sequoia, dd_beech)

        # load sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        # put branch twig leaf
        o_sequoia.set_leaf("value")
        s_vein = "value"
        o_value = "smoke"
        ls_branchtwigleaf=["cellline_a","run_a","well_a","nm_120"]
        o_sequoia.put(s_vein=s_vein, o_value=o_value, ls_branchtwigleaf=ls_branchtwigleaf)
        self.assertEqual(o_sequoia, dd_birch)


    def test_ddtree_rename(self):
        # result
        dd_beech = {
            'cellline_c': {
                'run_c': {'well_a': {
                    'nm_120': {'input': [8, 17, 26]},
                    'nm_240': {'input': [35, 44, 53]},
                    'nm_360': {'input': [62, 71, 80]}}},
                'run_b': {'well_a': {
                    'nm_120': {'input': [5, 14, 23]},
                    'nm_240': {'input': [32, 41, 50]},
                    'nm_360': {'input': [59, 68, 77]}}},
                'run_a': {'well_a': {
                    'nm_120': {'input': [2, 11, 20]},
                    'nm_240': {'input': [29, 38, 47]},
                    'nm_360': {'input': [56, 65, 74]}}}},
            'cellline_a': {
                'run_c': {'well_a': {
                    'nm_120': {'input': [6, 15, 24]},
                    'nm_240': {'input': [33, 42, 51]},
                    'nm_360': {'input': [60, 69, 78]}}},
                'run_b': {'well_a': {
                    'nm_120': {'input': [3, 12, 21]},
                    'nm_240': {'input': [30, 39, 48]},
                    'nm_360': {'input': [57, 66, 75]}}},
                'run_a': {'well_a': {
                    'nm_120': {'input': [0, 9, 18]},
                    'nm_240': {'input': [27, 36, 45]},
                    'nm_360': {'input': [54, 63, 72]}}}},
            'cellline_b': {
                'run_c': {'well_a': {
                    'nm_120': {'input': [7, 16, 25]},
                    'nm_240': {'input': [34, 43, 52]},
                    'nm_360': {'input': [61, 70, 79]}}},
                'run_b': {'well_a': {
                    'nm_120': {'input': [4, 13, 22]},
                    'nm_240': {'input': [31, 40, 49]},
                    'nm_360': {'input': [58, 67, 76]}}},
                'run_a': {'well_a': {
                    'nm_120': {'input': [1, 10, 19]},
                    'nm_240': {'input': [28, 37, 46]},
                    'nm_360': {'input': [55, 64, 73]}}}}}

        # load sequoia
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        o_sequoia.set_leaf("value")
        # rename vein
        s_vein = "input"
        o_sequoia.rename(s_vein=s_vein)
        self.assertEqual(o_sequoia, dd_beech)


    def test_ddtree_twigpop(self):
        # result
        dd_beech = {
            'cellline_a': {
                'run_b': {'well_a': {'value': [57, 66, 75]}},
                'run_a': {'well_a': {'value': [54, 63, 72]}},
                'run_c': {'well_a': {'value': [60, 69, 78]}}},
            'cellline_c': {
                'run_b': {'well_a': {'value': [59, 68, 77]}},
                'run_a': {'well_a': {'value': [56, 65, 74]}},
                'run_c': {'well_a': {'value': [62, 71, 80]}}},
            'cellline_b': {
                'run_b': {'well_a': {'value': [58, 67, 76]}},
                'run_a': {'well_a': {'value': [55, 64, 73]}},
                'run_c': {'well_a': {'value': [61, 70, 79]}}}}
        #
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("ddtree_smoketest00.json")
        o_sequoia.json_read()
        # tree
        o_sequoia.set_leaf("well_a")  # set new twig level
        s_vein = "nm_360"  # set new vein content
        o_sequoia.twigpop(s_vein=s_vein)
        self.assertEqual(o_sequoia, dd_beech)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
