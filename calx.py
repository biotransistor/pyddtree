"""
title: calx.py
language: python 3.5

author: bue

description:
    ddtree.py compatible calculation objects library.

run::

    from pyddtree.ddtree import calx

"""
# python standard library
# bue 20151230: python standard library statistics
import copy
import math
import statistics
import sys
# python scipy library
import numpy as np
from scipy import stats


### internal method  ###
def strlist2reallist(s_tuple, o_type):
    """
    description:
      *internal method*:
      unpacks tuple string into and real typle
    """
    s_tuple = s_tuple.replace("(","")
    s_tuple = s_tuple.replace(")","")
    s_tuple = s_tuple.replace("[","")
    s_tuple = s_tuple.replace("]","")
    ls_tule = s_tuple.split(",")
    lr_tuple = [o_type(s_tuple) for s_tuple in ls_tule if len(s_tuple) > 0]
    tr_tuple = tuple(lr_tuple)

    # output
    return(tr_tuple)


### basic manipulation methodes ###
def count(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - count vein on leaf

    description:
      count number of vein values.
    """
    i_count = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    if (type(lr_value) == list):
        i_count = len(lr_value)
    elif (lr_value != None):
        i_count = 1

    # output
    return(i_count)


def leafmax(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - max vein on leaf

    description:
      leaf vein max value.
      https://docs.python.org/3.5/library/functions.html#max
    """
    r_max = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    if (type(lr_value) == list):
        r_max = max(lr_value)
    else:
        r_max = lr_value

    # output
    return(r_max)


def leafmin(o_ddtree, d_calxargs=None):
    """
    pyddtee calx compatible calcualtion implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - min vein on leaf

    description:
      leaf vein min value.
      https://docs.python.org/3.5/library/functions.html#min
    """
    r_min = None
    # extract d_calxargs
    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    if (type(lr_value) == list):
        r_min = min(lr_value)
    else:
        r_min = lr_value

    # output
    return(r_min)


def log2(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calcualtion implementation

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - log2 vein on leaf

    description:
      leaf vein log2 value.
      https://docs.scipy.org/doc/numpy/reference/generated/numpy.log2.html
    """
    lr_log2 = None
    # extract d_calxargs
    if (o_ddtree.b_verbose):
        print("Log2 process branch: {}".format(o_ddtree.ls_branch))

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    if (type(lr_value) == list):
        lr_log2 = list(np.log2(lr_value))
    else:
        lr_log2 = np.log2(lr_value)

    # output
    return(lr_log2)


def mirror(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calcualtion implementation

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - d_calxargs["r_low"] : lower boarder value.
        lower values then this will be mirrored back to this value.
        None will not mirror on the lower side. default is None.
      - d_calxargs["r_high"] : higherr boarder value.
        higher values then this will be mirrored back to this value.
        None will not mirror on the higher side. default is None.

    output:
      - mirror vein on leaf

    description:
      mirrored cut leaf vein.
      https://docs.scipy.org/doc/numpy/reference/generated/numpy.clip.html
    """
    lr_mirror = None
    # extract d_calxargs
    try:
        r_low = d_calxargs["r_low"]
    except:
        r_low = None
        if (o_ddtree.b_verbose):
            print("Warning calx.mirror: no explicite r_low provieded, min will stay min.")
    try:
        r_high = d_calxargs["r_high"]
    except:
        r_high = None
        if (o_ddtree.b_verbose):
            print("Warning calx.mirror: no explicite r_high provieded, max will stay max.")

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    if (r_low == None):
        r_low = min(lr_value)
    if (r_high == None):
        r_high = max(lr_value)
    lr_mirror = list(np.clip(lr_value, a_min=r_low, a_max=r_high))

    # output
    return(lr_mirror)


def trunc(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calcualtion implementation

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - d_calxargs["r_low"] = None: lower boarder value.
        lower values will be truncated.
        None will not truncate on the lower side. default is None.
      - d_calxargs["r_high"] = None: higher boarder value.
        higher values will be trunctated.
        None will not truncate on the higher side. default is None.
    output:
      - trunced vein on leaf

    description:
      trunc cut leaf vein.
    """
    lr_trunc = None
    # extract d_calxargs
    try:
        r_low = d_calxargs["r_low"]
    except:
        r_low = None
        if (o_ddtree.b_verbose):
            print("Warning calx.trunc: no explicite r_low provieded, min will stay min.")
    try:
        r_high = d_calxargs["r_high"]
    except:
        r_high = None
        if (o_ddtree.b_verbose):
            print("Warning calx.trunc: no explicite r_high provieded, max will stay max.")

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    if (r_low == None):
        r_low = min(lr_value)
    if (r_high == None):
        r_high = max(lr_value)
    lr_trunc = [i for i in lr_value if ((i > r_low) and (i <= r_high))]

    # output
    er_value = set(lr_value)
    er_trunc = set(lr_trunc)
    er_diff = er_value.difference(er_trunc)
    if (o_ddtree.b_verbose) and (len(er_diff) != 0):
        print("\nAt branch {} truncd {}".format(o_ddtree.ls_branch, er_diff))
    return(lr_trunc)


### karl friederich gauss ###
def cv(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein, specified via o_ddtree.set_leaf()

    output:
      - cv vein on leaf

    description:
      coefficient of variation. dimensionless measure.
    """
    r_cv = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_mean = statistics.mean(data=lr_value)
    r_stdev = statistics.stdev(data=lr_value)
    r_cv = r_stdev / abs(r_mean)

    # output
    return(r_cv)

def cvmad(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - o_ddtree.o_central: statistics.mean object

    output:
      - cvmad vein on leaf

    description:
      mad (mean absolute deviation) instead of solely mean based
      coefficient of variation calcuylation. dimensionless measure.
      mad calculation is by default based on mean, other
      central tendency functions (e.g. statistics.median) can be applied.
      https://en.wikipedia.org/wiki/Median_absolute_deviation
    """
    r_cvmad = None
    # extract d_calxargs

    # central tendency
    try:
       o_central = d_calxargs["o_central"]
    except:
       o_central = statistics.mean

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_central = o_central(data=lr_value)
    r_mad = sum(abs(np.array(lr_value)-o_central(lr_value))) / len(lr_value)
    r_cvmad = r_mad / abs(r_central)

    # output
    return(r_cvmad)


def kolmogorovsmirnov(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein, specified via o_ddtree.set_leaf()

    output:
      - kolmogorovsmirnov vein on leaf

    description:
      Kolmogorov-Smirnov test for goodness of fit of a normal distribution.
      https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kstest.html
    """
    # set output variable
    r_ks = None
    # extract d_calxargs

    # get input values
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_mean = statistics.mean(lr_value)
    ar_value = np.array(lr_value) - r_mean

    # calculate ks statistics
    # ks test statistic (max distcance): D = max|CDFdata - CDFref|
    # Normal distributed data will have a D closed to 0. pvalue as usual.
    # D  < 0.05 is rule of thumb normal distributed
    ar_ks = stats.kstest(rvs=ar_value, cdf="norm", alternative="two-sided")

    # output
    if (o_ddtree.b_verbose):
        print("KS:", ar_ks)
    s_ks = str(tuple(ar_ks))
    return(s_ks)


def kurtosis(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein, specified via o_ddtree.set_leaf()
      - d_calxargs["b_fisher"] = True.
      - d_calxargs["b_bias"] = True.
      - d_calxargs["s_nanpolicy"] = propagate.

    output:
      - kurtosis vein on leaf. the normal value would be 3, though
        as b_fishe is by default set to True, the normal value is shifted 0.

    description:
      https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kurtosis.html

    note to dimesniosn less measure:
      historical kurtosis. the 4th standardized central moment.
      https://en.wikipedia.org/wiki/Standardized_moment
    """
    # extract d_calxargs
    # pearson or fisher
    try:
        b_fisher = d_calxargs["b_fisher"]
    except:
        b_fisher = True
    # bias
    try:
        b_bias = d_calxargs["b_bias"]
    except:
        b_bias = True
    # nan policy
    try:
        s_nanpolicy=d_calxargs["s_nanpolicy"]
    except:
        s_nanpolicy="propagate"

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_kurtosis = stats.kurtosis(lr_value, fisher=b_fisher, bias=b_bias) #nan_policy=s_nanpolicy

    # output
    return(r_kurtosis)


def mad(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - o_ddtree.o_central: statistics.mean object

    output:
      - mad vein on leaf

    description:
      mad absolute deviation calculation.
      default is mean absolute deviation,
      other central tendency functions can be applied.
    """
    r_mad = None
    # extract d_calxargs

    # central tendency
    try:
       o_central = d_calxargs["o_central"]
    except:
       o_central = statistics.mean

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_mad = sum(abs(np.array(lr_value)-o_central(lr_value))) / len(lr_value)

    # output
    return(r_mad)


def mean(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - mean vein on leaf

    description:
      https://docs.python.org/3/library/statistics.html#statistics.mean

    note to dimesniosn less measure:
      the 1st standardized central moment mean of mean is always 0.
      https://en.wikipedia.org/wiki/Standardized_moment
    """
    r_mean = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_mean = statistics.mean(data=lr_value)

    # output
    return(r_mean)


def median(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - median vein on leaf

    description:
      https://docs.python.org/3/library/statistics.html#statistics.median
    """
    r_median = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_median = statistics.median(data=lr_value)

    # output
    return(r_median)


def mode(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - mode vein on leaf

    description:
      mode - most common value of discrete data.
      https://docs.python.org/3/library/statistics.html#statistics.mode
    """
    r_mode = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    try:
        r_mode = statistics.mode(data=lr_value)
    except statistics.StatisticsError:
        r_mode = "none"

    # output
    return(r_mode)


def s3mirror(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calcualtion implementation

    input:
      - o_ddtree: ddtree obj
      - d_calxargs["r_low"] = 3.0: lower boarder sigma value.
        lower values then this will be mirrored back to this value.
      - d_calxargs["r_high"] = 3.0: higher boarder sigma value.
        higher values then this will be mirrored back to this value.
    output:
      - mirror vein on leaf

    description:
      o_ddtree.s_vein, specified via o_ddtree.set_leaf(), leaf morror value.
      https://docs.scipy.org/doc/numpy/reference/generated/numpy.clip.html
    """
    lr_mirror = None
    # extract d_calxargs
    try:
        r_low = d_calxargs["r_low"]
    except:
        r_low = 3.0
    try:
        r_high = d_calxargs["r_high"]
    except:
        r_high = 3.0

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_mean = statistics.mean(lr_value)
    r_min = r_mean - statistics.stdev(lr_value) * r_low
    r_max = r_mean + statistics.stdev(lr_value) * r_high
    o_mirror = np.clip(lr_value, a_min=r_min, a_max=r_max)
    lr_mirror = list(o_mirror)

    # output
    return(lr_mirror)


def outlier(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - outlier vein on leaf

    description:
      calcualte procent outlier.
      as outlier is defiend every value more or equal then 2 stdev
      or less or equal then 2 stdev from mean.
    """
    r_ol = None

    # extract d_calxargs
    # get input values
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]

    # calculate ol
    r_mean = statistics.mean(data=lr_value)
    r_stdev = statistics.stdev(data=lr_value)
    r_min = r_mean - 2 * r_stdev
    r_max = r_mean + 2 * r_stdev
    lr_ol = [r for r in lr_value if ((r <= r_min) or (r >= r_max))]
    r_ol = len(lr_ol)/len(lr_value)

    # output
    return(r_ol)


def skewness(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein, specified via o_ddtree.set_leaf()
      - d_calxargs["b_fisher"] = True.
      - d_calxargs["b_bias"] = True.
      - d_calxargs["s_nanpolicy"] = propagate.

    output:
      - kurtosis vein on leaf. The normal value would be 2,
        though is by the software reset to 0.

    description:
      https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.skew.html

    note to dimesniosn less measure:
      the 3rd standardized central moment.
      https://en.wikipedia.org/wiki/Standardized_moment
    """
    # extract d_calxargs
    # bias
    try:
        b_bias = d_calxargs["b_bias"]
    except:
        b_bias = True
    # nan policy
    try:
        s_nanpolicy = d_calxargs["s_nanpolicy"]
    except:
        s_nanpolicy = "propagate"

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_skew = stats.skew(lr_value, bias=b_bias) # nan_policy=s_nanpolicy

    # output
    return(r_skew)


def stdev(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - stdev vein on leaf

    description:
      https://docs.python.org/3/library/statistics.html#statistics.stdev
      http://www.mathsisfun.com/data/standard-deviation.html
    """
    r_stdev = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_stdev = statistics.stdev(data=lr_value)

    # output
    return(r_stdev)


def q001trunc(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calcualtion implementation

    input:
      - o_ddtree : ddtree obj
      - d_calxargs["r_low"] = 0.001: lower boarder quantile value.
        lower values will be truncated.
      - d_calxargs["r_high"] = 0.999: higher boarder quantile value.
        higher values will be trunctated.
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - q001trunc vein on leaf

    description:
      https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.mstats.mquantiles.html
    """
    # extract d_calxargs
    try:
        r_low = d_calxargs["r_low"]
    except:
        r_low = 0.001
    try:
        r_high = d_calxargs["r_high"]
    except:
        r_high = 0.999

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    o_trunc = stats.mstats.mquantiles(lr_value, prob=[r_low, r_high])
    r_low = o_trunc[0]
    r_high = o_trunc[1]
    lr_trunc = [i for i in lr_value if (i > r_low) and (i <= r_high)]

    # output
    er_value = set(lr_value)
    er_trunc = set(lr_trunc)
    er_diff = er_value.difference(er_trunc)
    if (o_ddtree.b_verbose) and (len(er_diff) != 0):
        print("\nAt bracnch {} quantile hard trunc {}".format(o_ddtree.ls_branch, er_diff))
    return(lr_trunc)


def s3trunc(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calcualtion implementation

    input:
      - o_ddtree : ddtree obj
      - d_calxargs["r_low"] = 3.0: lower bound factor of sigma clipping.
        lower values then this will be truncated.
      - d_calxargs["r_high"] = 3.0: higher boarder sigma value.
        higher values then this will truncated.
      - o_ddtree.s_vein, specified via o_ddtree.set_leaf()

    output:
      - s3trunc vein on leaf

    description:
      https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.sigmaclip.html
    """
    lr_trunc = None
    # extract d_calxargs
    try:
        r_low = d_calxargs["r_low"]
    except:
        r_low = 3.0
    try:
        r_high = d_calxargs["r_high"]
    except:
        r_high = 3.0

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    o_trunc = stats.sigmaclip(lr_value, low=r_low, high=r_high)
    lr_trunc = list(o_trunc[0])

    # output
    if (o_ddtree.b_verbose):
        er_value = set(lr_value)
        er_trunc = set(lr_trunc)
        er_diff = er_value.difference(er_trunc)
        if (len(er_diff) != 0):
            print("\nAt branch {} trunc by sigma {}".format(o_ddtree.ls_branch, er_diff))
        else:
            print("\nAt branch {} nothing truncd by sigma".format(o_ddtree.ls_branch))
    return(lr_trunc)


def variance(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()

    output:
      - variance vein on leaf

    description:
      https://docs.python.org/3/library/statistics.html#statistics.variance

    note to dimensionless measure:
      variance is equal to the 2nd standardized central moment.
      the normal value is 1.
      https://en.wikipedia.org/wiki/Standardized_moment
    """
    r_variance = None
    # extract d_calxargs

    # calcualtion
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    r_variance = statistics.variance(data=lr_value)

    # output
    return(r_variance)


### claude shannon ###
def bina(d_calxargs=None):  # o_ddtree
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - d_calxargs["o_bin"] = L: number of bins value. integer or string.
        e.g. L large = 72 bins. valid strings are: trinary, ternary, XXS,
        quintal, octal, heximal, sigma, hexadecimal, S, M, L, XL, XXL, XXXL.

    output:
      - bin integer value.

    description:
      transform legal integer or alphabetic bin value into integer value.
    """
    # o_bin
    try:
        o_bin = d_calxargs["o_bin"]
    except KeyError:
        sys.exit("Error calx.bin: d_calxargs['o_bin'] value missing.")

    if (type(o_bin) == int):
        pass
    elif (type(o_bin) == float):
        o_bin = int(o_bin)
    elif (type(o_bin) == str):
        if (o_bin.lower() == "binary"):
            # binary
            o_bin = 2
        elif (o_bin.lower() == "trinary") or\
             (o_bin.lower() == "ternary") or\
             (o_bin.upper() == "XXS"):
            # ternary (3**1)*(2**0)
            o_bin = 3
        elif (o_bin.lower() == "quintal"):
            # quintal
            o_bin = 4
        elif (o_bin.lower() == "octal"):
            # octal
            o_bin = 8
        elif (o_bin.lower() == "heximal") or\
             (o_bin.lower() == "senary") or\
             (o_bin.lower() == "sigma"):
            # heximal
            o_bin = 6
        elif (o_bin.upper() == "XS"):
            # (3**2)*(2**0)
            o_bin = 9
        elif (o_bin.lower() == "hexadecimal"):
            # hexadecimal
            o_bin = 16
        elif (o_bin.upper() == "S"):
            # (3**2)*(2**1)
            o_bin = 18
        elif (o_bin.upper() == "M"):
            # (3**2)*(2**2)
            o_bin = 36
        elif (o_bin.upper() == "L"):
            # (3**2)*(2**3)
            o_bin = 72
        elif (o_bin.upper() == "XL"):
            # (3**2)*(2**4)
            o_bin = 144
        elif (o_bin.upper() == "XXL"):
            # (3**2)*(2**5)
            o_bin = 288
        elif (o_bin.upper() == "XXXL"):
            # (3**2)*(2**6)
            o_bin = 576
        else:
            sys.exit("Error calx.bin: unknown bin number string {}.".format(o_bin))
    else:
        sys.exit("Error calx.bin: can not handel this o_bin number object {} with varable type {}. Alowed types are integer, float and some string".format(o_bin, type(o_bin)))
    # output
    return(o_bin)


def frequency(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - d_calxargs["o_bin"] = L: number of bins value. integer or string.
        e.g. L large = 72 bins. valid strings are: trinary, ternary, XXS,
        quintal, octal, heximal, sigma, hexadecimal, S, M, L, XL, XXL, XXXL.
      - d_calxargs["o_max"] = math.ceil(o_max): maximum value.
        real of vein label. e.g. max pixle intensity 2**12 = 4096. leafmax.
      - d_calxargs["o_min"] = math.ceil(o_min): minimum value.
        real or vein label. e.g. min pixle intensity 0. leafmin.

    output:
      - frequency vein on leaf

    description:
      calcullate bin frequency over an o_bin even splited distribution.
      https://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram.html
    """
    lr_frequency = None
    # extract d_calxargs
    # o_max
    try:
        o_max = d_calxargs["o_max"]
        if (type(o_max) == str):
            o_max = o_ddtree.d_leaf[o_max]
        else:
            o_max = math.ceil(o_max)
    except KeyError:
        sys.exit("Error calx.freqency: non o_max specified.")
    # o_min
    try:
        o_min = d_calxargs["o_min"]
        if (type(o_min) == str):
            o_min = o_ddtree.d_leaf[o_min]
        else:
            o_min = math.ceil(o_min)
    except KeyError:
        o_min = 0
        if (o_ddtree.b_verbose):
            print("Warning calx.frequency: no o_mean specified. o_mean value 0 assumed.")
    # o_bin
    try:
        o_bin = d_calxargs["o_bin"]
    except KeyError:
        o_bin = 'L'
    # translate object to integer
    d_calxargs["o_bin"] = o_bin
    i_bin = bina(d_calxargs=d_calxargs)

    # calculation
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    tar_result = np.histogram(a=lr_value, bins=i_bin, range=(o_min,o_max), density=False)  # bins; weigths
    ar_frequency = tar_result[0] / sum(tar_result[0])
    #print("Frequency :", ar_frequency)

    # output
    s_frequency = (str(tuple(ar_frequency)))
    return(s_frequency)


def word2bit(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: word leaf, specified via o_ddtree.set_leaf()
      - d_calxargs["r_base"] = 2: base. default base is bit and as such 2.
        other bases can be choosen. 3 for tet or e for nat.

    output:
      - word2bit vein on leaf

    description:
      - translate word value in to bit (exponent) vale
    """
    r_bit = None
    # extract d_calxargs
    try:
        r_exponent = o_ddtree.d_leaf[o_ddtree.s_vein]  # bit vein
    except KeyError:
        sys.exit("Error calx.bit2word: at branchtwig {} {} no bit (exponent) leaf found {}. Specified vein via o_ddtree.set_leaf().".format(o_ddtree.ls_branch, o_ddtree.s_vein, o_ddtree.d_leaf.keys()))

    # gest exponent
    try:
        r_base = d_calxargs["r_base"]
    except KeyError:
        r_base = 2

    # calcualtion
    r_bit = math.log(r_exponent, r_base)

    # output
    return(r_bit)


def bit2word(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: bit (exponent) leaf, specified via o_ddtree.set_leaf()
      - d_calxargs["r_base"] = 2: base. default base is bit and as such 2.
        other bases can be choosen. 3 for tet or e for nat.

    output:
      - bit2word vein on leaf

    description:
      - translate bit value in to word vale
    """
    r_word = None
    # extract d_calxargs
    try:
        r_exponent = o_ddtree.d_leaf[o_ddtree.s_vein]  # bit vein
    except KeyError:
        sys.exit("Error calx.bit2word: at branchtwig {} {} no bit (exponent) leaf found {}. Specified vein via o_ddtree.set_leaf().".format(o_ddtree.ls_branch, o_ddtree.s_vein, o_ddtree.d_leaf.keys()))

    # get exponent
    try:
        r_base = d_calxargs["r_base"]
    except KeyError:
        r_base = 2

    # calcualtion
    r_word = r_base**r_exponent

    # output
    return(r_word)


def bit2languagefraction(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatible calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: bit entropy uncertainity (exponent) leaf, specified via o_ddtree.set_leaf()
      - d_calxargs["r_base"] = 2: base. default base is bit and as such 2.
        other bases can be choosen. 3 for tet or e for nat.
        naturealy this have to be the same base the o_ddtree.s_vein bit base.
      - d_calxargs["o_bin"] = L: number of bin. equivalent to bit language capacity.
        e.g. L large = 72 bins = 72 bit language capacity = 2**72 word language capacity.
        valid strings are: trinary, ternary, XXS,
        quintal, octal, heximal, sigma, hexadecimal, S, M, L, XL, XXL, XXXL.

    output:
      - bit2languagefraction vein on leaf

    description:
      translate bit entropy uncertainity into language fracton.
    """
    # extract d_calxargs
    # o_bin
    try:
        o_bin = d_calxargs["o_bin"]
    except KeyError:
        o_bin = 'L'
    # translate object to integer
    d_calxargs["o_bin"] = o_bin
    i_bitcapacity = bina(d_calxargs=d_calxargs)
    # get exponent
    try:
        r_base = d_calxargs["r_base"]
    except KeyError:
        r_base = 2

    # extract form leaf
    # s_vein
    try:
        r_bitunceratainity = o_ddtree.d_leaf[o_ddtree.s_vein]  # bit vein
    except KeyError:
        sys.exit("Error calx.bit2languagefraction: at branchtwig {} {} no bit entropy uncentainity (exponent) leaf found {}. \
Specified vein via o_ddtree.set_leaf().".format(o_ddtree.ls_branch, o_ddtree.s_vein, o_ddtree.d_leaf.keys()))

    # calcualtion
    r_languagefraction = r_base ** (r_bitunceratainity - i_bitcapacity)

    # output
    return(r_languagefraction)


def entropy_shannon(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: frequency leaf, specified via o_ddtree.set_leaf()

    output:
      - entropy_shannon vein on leaf

    description:
      calculates shannon entropy over an i_bin even bin splited distribution.
    """
    # extract d_calxargs
    # get frequency values
    try:
        s_frequency = o_ddtree.d_leaf[o_ddtree.s_vein]
        tr_frequency = strlist2reallist(s_frequency, float)
    except KeyError:
        sys.exit("Error calx.entropy_shannon: at branchtwig {} {} no frequency leaf found {}. Run o_ddtree.frequency().".format(o_ddtree.ls_branch, o_ddtree.s_vein, o_ddtree.d_leaf.keys()))

    # calcualtion
    r_entropy = 0
    for r_frequency in tr_frequency:
        if r_frequency > 0:
            r_entropy = r_entropy + r_frequency * np.log(r_frequency)  # natural logarithm
    r_entropy = -(r_entropy)

    # output
    return(r_entropy)


def entropy_ginisimpson(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: frequency leaf, specified via o_ddtree.set_leaf()

    output:
      - entropy_shannon vein on leaf

    description:
      calculates gini-simpson entropy over an i_bin even bin splited distribution.
    """
    # extract d_calxargs
    # get frequency values
    try:
        s_frequency = o_ddtree.d_leaf[o_ddtree.s_vein]
        tr_frequency = strlist2reallist(s_frequency, float)
    except KeyError:
        sys.exit("Error calx.entropy_ginisimpson: at branchtwig {} no frequency leaf found {}. Run o_ddtree.frequency().".format(o_ddtree.ls_branch, o_ddtree.d_leaf.keys()))

    # calcualtion
    r_entropy = 0
    for r_frequency in tr_frequency:
        r_entropy = r_entropy + r_frequency ** 2
    r_entropy = 1 - r_entropy

    # output
    return(r_entropy)


def entropy_raoexpression(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: frequency leaf, specified via o_ddtree.set_leaf()

    output:
      - entropy_shannon vein on leaf

    description:
      calculates rao's quadratic entropy over an i_bin even bin splited distribution.
    """
    # bue 20160330: the waith should in general be ternary over the whole rage,
    # with the possibily to set it to what ever.
    # number of bin are length of the frequency leaf devide it
    # by the sugested number, give a error wenn not ganzzahlig devidable.

    # extract d_calxargs
    # get i_setp which define the total weight
    # deafult is 6, one weight per sigma
    try:
        i_step = d_calxargs["i_step"]
    except KeyError:
        i_step = 6

    # get frequency values
    try:
        s_frequency = o_ddtree.d_leaf[o_ddtree.s_vein]
        tr_frequency = strlist2reallist(s_frequency, float)
    except KeyError:
        sys.exit("Error calx.entropy_raoexpress: at branchtwig {} no frequency leaf found {}. Run o_ddtree.frequency().".format(o_ddtree.ls_branch, o_ddtree.d_leaf.keys()))

    # calcualtion
    r_entropy = 0
    n = len(tr_frequency)  # number of species
    for j in range(n):
        r_jfrequency = tr_frequency[j]
        for i in range(j+1, n):
            # frequency r and distance value dij
            r_ifrequency = tr_frequency[i]
            r_dij = (i - j) / i_step
            r_entropy = r_entropy + r_dij * r_ifrequency * r_jfrequency
    # bue 20160323: do i only get the half because it is the sum of the sum?
    r_entropy = r_entropy*2

    # output
    return(r_entropy)

# def entropy_raobinary()
# this is same as ginisimpson, since all classes are major calsses and the distance between each class is the same.

# def entropy_raoternatry(o_ddtree, d_calxargs):


def vocabulary(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: value leaf, specified via o_ddtree.set_leaf()
      - d_calxargs["o_max"] = math.ceil(o_max): maximum value.
        real of vein label. e.g. max trait pixle intensity 2**12 = 4096. leafmax.
      - d_calxargs["o_min"] = math.ceil(o_min): minimum value.
        real or vein label. e.g. min trait pixle intensity 0. leafmin.
      - d_calxargs["ti_binrange"] = (1,128): bin (bit langauge capacity) range to be scanned,
        form the language (trait) under investigation. default is (1,128).
      - d_calxargs["o_entropy"] = entropy_shannon: entropy measure methode.
        default is shannon, mother of all the entropy measure methods.

    output:
      - vocabulary vein on leaf.

    description:
        function itertively increasuing numbers of bins (bit language capacity) over the
        d_calxargs["ti_binrange"] given range, thereby each time calculats
        bit entropy uncetainity by the choosen d_calxargs["o_entropy"] measure methode.
    """
    lr_entropy = None

    # extract o_ddtree.set_leaf
    s_vein = o_ddtree.s_vein

    # extract d_calxargs
    d_frequencycalxargs = {}
    d_entropycalxargs = {}
    ### o_max ###
    try:
        o_max = d_calxargs["o_max"]
        if (type(o_max) == str):
            o_max = o_ddtree.d_leaf[o_max]
        else:
            o_max = math.ceil(o_max)
    except KeyError:
        sys.exit("Error calx.vocabulary: non o_max specified.")
    # out
    d_frequencycalxargs.update({"o_max": o_max})
    ### o_min ###
    try:
        o_min = d_calxargs["o_min"]
        if (type(o_min) == str):
            o_min = o_ddtree.d_leaf[o_min]
        else:
            o_min = math.ceil(o_min)
    except KeyError:
        o_min = 0
        if (o_ddtree.b_verbose):
            print("Warning calx.vocabulary: no o_min specified. o_min value 0 assumed.")
    # out
    d_frequencycalxargs.update({"o_min": o_min})
    ### ti_binrange ###
    try:
        ti_binrange = d_calxargs["ti_binrange"]
    except KeyError:
        ti_binrange = (1,128)
    # out
    o_binrange = range(ti_binrange[0],ti_binrange[1]+1)
    ### o_entropy ###
    try:
        o_entropy = d_calxargs["o_entropy"]
    except KeyError:
        o_entropy = entropy_shannon

    # store original veins
    # freqency
    #try:
    #    s_frequencybackup = o_ddtree.d_leaf["frequency"]
    #except KeyError:
    #    s_frequencybackup = None
    # entropy
    #try:
    #    r_entropybackup = o_ddtree.d_leaf[o_entropy.__name__]
    #except KeyError:
    #    r_entropybackup = None

    # calculation
    lr_output = []
    for i_bin in o_binrange:
        # bue 20160729: maybe not most RAM efficient,
        # but call by reference obj would make a str and rcl mplementation hardcore.
        o_ddclone = copy.deepcopy(o_ddtree)
        # delete frequency and entropy vein
        o_ddclone.set_leaf("frequency")
        o_ddclone.delete(ls_branchtwigleaf=o_ddtree.ls_branch)
        o_ddclone.set_leaf(o_entropy.__name__)
        o_ddclone.delete(ls_branchtwigleaf=o_ddtree.ls_branch)
        # recall original leaf s_vein
        o_ddclone.set_leaf(s_vein)
        # get frequency values
        d_frequencycalxargs.update({"o_bin": i_bin})
        o_ddclone.calx(frequency, d_calxargs=d_frequencycalxargs, ls_branchtwigleaf=o_ddtree.ls_branch)
        # get entropy
        o_ddclone.set_leaf("frequency")
        o_ddclone.calx(o_entropy, d_calxargs=d_entropycalxargs, ls_branchtwigleaf=o_ddtree.ls_branch)
        o_ddclone.set_leaf(o_entropy.__name__)
        lr_entropy = o_ddclone.get(ls_branchtwigleaf = o_ddtree.ls_branch)
        if (len(lr_entropy) != 1):
            sys.exit("Error calx.vocabulary: i want my entropy back! {}".format(lr_entropy))
        r_entropy = lr_entropy[0]
        # populate output
        lr_output.append(r_entropy)

    # recall original leaf s_vein
    #o_ddclone.set_leaf(s_vein)

    # recall original veins
    # frequency
    #if (s_frequencybackup != None):
    #    o_ddtree.d_leaf["frequency"] = s_frequencybackup
    # entropy
    #if (r_entropybackup != None):
    #    o_ddtree.d_leaf[o_entropymm.__name__] = r_entropybackup

    # output
    return(lr_output)


### gate ###
# bue: this is hardcore, used in species gate.
# bue 2016-07-20: should maybe be able to set the central where the bimodal distro break is.
def binary(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree: ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - d_calxargs["o_sigma0"] = 0: min value. real or vein label.
        e.g. min pixle log2 intensity log2(1) = 0. leafmin.
      - d_calxargs["o_sigma6"] max value.  real or vein label. e.g. leafmax.
      - d_calxargs["s_central"] = "sigma2": binary border.
        possible values: "sigma2" oder "mean".

    output:
      - binary vein on leaf

    description:
      transforms values in binary value. binary frequency.
      the binary value is eiter by the mean
      or by sigma2 which is -1 sigam from the mean.
    """
    li_value = None

    # extract d_calxargs
    # r_sigma6
    try:
        o_sigma6 = d_calxargs["o_sigma6"]
        if (type(o_sigma6) == str):
            o_sigma6 = o_ddtree.d_leaf[o_sigma6]
        #r_sigma6 = math.ceil(o_sigma6)
        r_sigma6 = float(o_sigma6)
    except KeyError:
        sys.exit("Error calx.binary: non o_sigma6 specified.")

    # r_sigma0
    try:
        o_sigma0 = d_calxargs["r_sigma0"]
        if (type(o_sigma0) == str):
            o_sigma0 = o_ddtree.d_leaf[o_sigma0]
        #i_sigma0 = math.ceil(o_sigma0)
        r_sigma0 = float(o_sigma0)
    except KeyError:
        r_sigma0 = 0
        if (o_ddtree.b_verbose):
            print("Warning calx.binary: no o_sigma0 specified. o_sigma0 value 0 assumed.")

    # r_central
    try:
        s_central = d_calxargs["s_central"]
    except KeyError:
        s_central = "sigma2"
        if (o_ddtree.b_verbose):
            print("Warning calx.binary: no s_central specified. s_cetral sigma2 assumed.")
    # set value
    if (s_central == "sigma2"):
        r_central = (r_sigma6 - r_sigma0)/3 + r_sigma0
    elif (s_central == "mean"):
        r_central = (r_sigma6 - r_sigma0)/2 + r_sigma0
    else:
        sys.exit("Error calx.binary: unknowen s_central {}, knowen are sigma2 and mean.".format(s_central))

    # calculation
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    li_value = [0 if r_value < r_central else 1 for r_value in lr_value]
    # output
    return(li_value)


# bue: this is hardcore, used in species gate.
def ternary(o_ddtree, d_calxargs=None):
    """
    pyddtree calx compatoble calculation implementation.

    input:
      - o_ddtree : ddtree obj
      - o_ddtree.s_vein: specified via o_ddtree.set_leaf()
      - d_calxargs["o_sigma0"] = 0: min value. real or vein label.
        e.g. min pixle log2 intensity log2(1) = 0. leafmin.
      - d_calxargs["o_sigma6"] max value.  real or vein label. e.g. leafmax.
      - d_calxargs["s_central"] = "sigma2": binary border.
        possible values: "sigma2" oder "mean".

    output:
      - binary vein on leaf

    description:
      transforms values in ternary frequency value by two sigma (0,1,2).
    """
    # bue: balance ternay skiped for once
    li_value = None

    # extract d_calxargs
    # r_sigma6
    try:
        o_sigma6 = d_calxargs["o_sigma6"]
        if (type(o_sigma6) == str):
            o_sigma6 = o_ddtree.d_leaf[o_sigma6]
        r_sigma6 = float(o_sigma6)
    except KeyError:
        sys.exit("Error calx.ternary: non o_sigma6 specified.")

    # r_sigma0
    try:
        o_sigma0 = d_calxargs["r_sigma0"]
        if (type(o_sigma0) == str):
            o_sigma0 = o_ddtree.d_leaf[o_sigma0]
        r_sigma0 = float(o_sigma0)
    except KeyError:
        r_sigma0 = 0
        if (o_ddtree.b_verbose):
            print("Warning calx.ternary: no o_sigma0 specified. o_sigma0 value 0 assumed.")

    # r_central
    r_sigma2 = (r_sigma6 - r_sigma0)/3 + r_sigma0
    r_sigma4 = 4*(r_sigma6 - r_sigma0)/6 + r_sigma0

    # calculation
    lr_value = o_ddtree.d_leaf[o_ddtree.s_vein]
    li_value = [0 if r_value < r_sigma2 else 1 if r_value <= r_sigma4 else 2 for r_value in lr_value]
    # output
    return(li_value)


### plot output ###
# bue 20181118: is hist functions below is from pyheterogenes
def hist(d_calxargs):
    """
    take leave part specified by s_input. makes histograms
    input:
        d_calxargs["s_input"] = "frequency"
        d_calxargs["b_png"] = False
    """
    # extract d_calxargs
    d_leaf = d_calxargs["d_leaf"]
    ls_branch = d_calxargs["ls_branch"]
    # ls_outpop
    try:
       s_input = d_calxargs["s_input"]
    except KeyError:
       s_input = "frequency"
    # b_png
    try:
       b_png = d_calxargs["b_png"]
    except KeyError:
       b_png = False

    # bue 20151112: this plot out is a hack
    li_input = d_calxargs["d_leaf"][s_input]
    o_fig = plt.figure()
    ax = o_fig.add_subplot(111)
    ax.bar(height=li_input, left=range(len(li_input)))
    #ax.set_xlim(0,64)
    ax.set_ylim(0,1)
    if b_png :
        s_out = "_".join(d_calxargs["ls_branch"])
        s_out = s_out + ".png"
        plt.savefig(s_out)
    else:
        plt.show()
    return(None)
