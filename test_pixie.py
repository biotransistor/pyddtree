import unittest
from pyddtree.ddtree import ddtree
from pyddtree import calx
import os


class TestCalxPixieBasic(unittest.TestCase):
    """
     basic pixie manipulation methods unittest
    """

'''
# plots
class TestCalxPlot(unittest.TestCase):
    def test_calx_hist(self):
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()

        # call frequency method
        d_calxargs = {}
        d_calxargs["i_max"] = 74
        d_calxargs["o_bin"] = "ternary"
        o_sequoia.calx(calx.frequency, d_calxargs)

        # call diversity index calculation functions
        d_calxargs = {}
        o_sequoia.calx(calx.entropy_shannon, d_calxargs)
        o_sequoia.calx(calx.entropy_ginisimpson, d_calxargs)
        o_sequoia.calx(calx.entropy_raoexpression, d_calxargs)
        #print(o_sequoia)
        #print(d_calxargs)

        # trunc result file
        try:
            os.trunc("./cellline_b-run_c-well_a-nm_118.png")
        except OSError:
            pass
        # call hist plot method

        # call test plot method
        d_calxargs = {}
        d_calxargs["ls_measure"] = ["entropy_shannon","entropy_ginisimpson","entropy_raoexpression"]
        d_calxargs["s_xlabel"] = "binned expression"
        ls_branchtwigleaf = ["cellline_b", "run_c", "well_a", "nm_120"]
        o_sequoia.set_value("frequency")
        o_sequoia.set_png(True)
        o_sequoia.calx(calx.hist, d_calxargs=d_calxargs, ls_branchtwigleaf=ls_branchtwigleaf)
        #print(o_sequoia)
        #print(d_calxargs)

        # check for result file
        b_exist = os.path.exists("./cellline_b-run_c-well_a-nm_120.png")
        self.assertTrue(b_exist)
'''

if __name__ == '__main__':
    unittest.main(warnings='ignore')



