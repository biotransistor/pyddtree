import unittest
from pyddtree.ddtree import ddtree
from pyddtree import calx
import numpy as np


class TestCalxBasic(unittest.TestCase):
    """
     basic calx manipulation methods unittest
    """
    def test_calx_count(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'count': 3,
                            'value': [31, 40, 49]},
                        'nm_360': {
                            'count': 3,
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'count': 3,
                            'value': [4, 13, 22]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.count, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_leafmax(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'leafmax': 49,
                            'value': [31, 40, 49]},
                        'nm_360': {
                            'leafmax': 76,
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'leafmax': 22,
                            'value': [4, 13, 22]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.leafmax, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_leafmin(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'leafmin': 31,
                            'value': [31, 40, 49]},
                        'nm_360': {
                            'leafmin': 58,
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'leafmin': 4,
                            'value': [4, 13, 22]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs ={}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.leafmin, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_log2(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'log2': [4.9541963103868749, 5.3219280948873626, 5.6147098441152083],
                            'value': [31, 40, 49]},
                        'nm_360': {
                            'log2': [5.8579809951275719, 6.0660891904577721, 6.2479275134435852],
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'log2': [2.0, 3.7004397181410922, 4.4594316186372973],
                            'value': [4, 13, 22]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs ={}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.log2, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_mirror(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'mirror': [32, 40, 49],
                            'value': [31, 40, 49]},
                        'nm_360': {
                            'mirror': [58, 64, 64],
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'mirror': [32, 32, 32],
                            'value': [4, 13, 22]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs ={}
        d_calxargs["r_low"] = 32
        d_calxargs["r_high"] = 64
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.mirror, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_trunc(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {'value': [31, 40, 49],
                                   'trunc': [40, 49]},
                        'nm_120': {'value': [4, 13, 22],
                                   'trunc': []},
                        'nm_360': {'value': [58, 67, 76],
                                   'trunc': [58]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs ={}
        d_calxargs["r_low"] = 32
        d_calxargs["r_high"] = 64
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.trunc, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


class TestCalxGauss(unittest.TestCase):
    """
    gaussian normal distribution calx methods unittestx
    """
    def test_calx_cv(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'value': [4, 13, 22],
                            'cv': 0.6923076923076923},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'cv': 0.225},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'cv': 0.13432835820895522}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set remove args
        d_calxargs = {}
        # call remove method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.cv, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_cvmad(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'value': [58, 67, 76],
                            'cvmad': 0.089552238805970144},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'cvmad': 0.14999999999999999},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'cvmad': 0.46153846153846156}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set remove args
        d_calxargs = {}
        # call remove method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.cvmad, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_kolmogorovsmirnov(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'value': [4, 13, 22],
                            'kolmogorovsmirnov': '(0.33333333333333337, 0.89277833725010836)'},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'kolmogorovsmirnov': '(0.33333333333333337, 0.89277833725010836)'},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'kolmogorovsmirnov': '(0.33333333333333337, 0.89277833725010836)'}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        #np.random.seed(987654321)
        o_sequoia.calx(calx.kolmogorovsmirnov, d_calxargs=d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_kurtosis(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'value': [4, 13, 22],
                            'kurtosis': -1.5},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'kurtosis': -1.5},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'kurtosis': -1.5}}}}}

        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.kurtosis, d_calxargs=d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_mad(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'value': [31, 40, 49],
                            'mad': 6.0},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'mad': 6.0},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'mad': 6.0}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.mad, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_mean(self):
        dd_plum= {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'mean': 67,
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'mean': 13,
                            'value': [4, 13, 22]},
                        'nm_240': {
                            'mean': 40,
                            'value': [31, 40, 49]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.mean, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_median(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'value': [58, 67, 76],
                            'median': 67},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'median': 13},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'median': 40}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.median, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_mode(self):
        dd_plum= {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'mode': None,
                            'value': [58, 67, 76]},
                        'nm_120': {
                            'mode': None,
                            'value': [4, 13, 22]},
                        'nm_240': {
                            'mode': None,
                            'value': [31, 40, 49]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.mode, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_s3mirror(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            's3mirror': [58, 67, 76],
                            'value': [58, 67, 76]},
                        'nm_120': {
                            's3mirror': [4, 13, 22],
                            'value': [4, 13, 22]},
                        'nm_240': {
                            's3mirror': [31, 40, 49],
                            'value': [31, 40, 49]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.s3mirror, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_outlier(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'value': [4, 13, 22],
                            'outlier': 0.0},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'outlier': 0.0},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'outlier': 0.0}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.outlier, d_calxargs=d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_skewness(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'value': [4, 13, 22],
                            'skewness': 0.0},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'skewness': 0.0},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'skewness': 0.0}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.skewness, d_calxargs=d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_stdev(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'value': [58, 67, 76],
                            'stdev': 9.0},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'stdev': 9.0},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'stdev': 9.0}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.stdev, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_q001trunc(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'q001trunc': [40, 49],
                            'value': [31, 40, 49]},
                        'nm_120': {
                            'q001trunc': [13, 22],
                            'value': [4, 13, 22]},
                        'nm_360': {
                            'q001trunc': [67, 76],
                            'value': [58, 67, 76]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.q001trunc, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_s3trunc(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            's3trunc': [58, 67, 76],
                            'value': [58, 67, 76]},
                        'nm_120': {
                            's3trunc': [4, 13, 22],
                            'value': [4, 13, 22]},
                        'nm_240': {
                            's3trunc': [31, 40, 49],
                            'value': [31, 40, 49]}}}}}

        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.s3trunc, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_variance(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {'value': [58, 67, 76],
                                   'variance': 81},
                        'nm_240': {'value': [31, 40, 49],
                                   'variance': 81},
                        'nm_120': {'value': [4, 13, 22],
                                   'variance': 81}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.variance, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


class TestCalxEntropy(unittest.TestCase):
    """
    entropy related calx methodes unittest
    """
    ### bina ###
    # bue: not tested.

    ### frequency ###
    def test_calx_frequency(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'value': [58, 67, 76],
                            'frequency': '(0.0, 1.0)'},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'frequency': '(0.33333333333333331, 0.66666666666666663)'},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'frequency': '(1.0, 0.0)'}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # set args
        d_calxargs = {}
        d_calxargs["o_max"] = 74
        d_calxargs["o_bin"] = "binary"
        # call method
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.frequency, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)

    ### bit word language ###
    def test_calx_word2bit(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'word': 4,
                            'word2bit': 2.0},
                        'nm_240': {
                            'word': 31,
                            'word2bit': 4.954196310386876},
                        'nm_360': {
                            'word': 76, 'word2bit': 6.247927513443586}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest02.json")
        o_sequoia.json_read()
        # call frequency method
        d_calxargs = {}
        #d_calxargs["o_max"] = 80
        #d_calxargs["o_bin"] = "binary"
        o_sequoia.set_leaf("word")
        o_sequoia.calx(calx.word2bit, d_calxargs)
        # call entropy method
        #d_calxargs = {}
        #o_sequoia.set_leaf("frequency")
        #o_sequoia.calx(calx.entropy_shannon, d_calxargs)
        # calx bit2languagefraction
        #d_calxargs = {}
        #o_sequoia.set_leaf("entropy_shannon")
        #o_sequoia.calx(calx.word2bit, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_bit2word(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'entropy_shannon': -0.0,
                            'frequency': '(0.0, 1.0)',
                            'value': [58, 67, 76],
                            'bit2word': 1.0},
                        'nm_240': {
                            'entropy_shannon': 0.63651416829481278,
                            'frequency': '(0.33333333333333331, 0.66666666666666663)',
                            'value': [31, 40, 49],
                            'bit2word': 1.5545684782022193},
                        'nm_120': {
                            'entropy_shannon': -0.0,
                            'frequency': '(1.0, 0.0)',
                            'value': [4, 13, 22],
                            'bit2word': 1.0}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # call frequency method
        d_calxargs = {}
        d_calxargs["o_max"] = 80
        d_calxargs["o_bin"] = "binary"
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.frequency, d_calxargs)
        # call entropy method
        d_calxargs = {}
        o_sequoia.set_leaf("frequency")
        o_sequoia.calx(calx.entropy_shannon, d_calxargs)
        # calx bit2languagefraction
        d_calxargs = {}
        o_sequoia.set_leaf("entropy_shannon")
        o_sequoia.calx(calx.bit2word, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_bit2languagefraction(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'value': [58, 67, 76],
                            'bit2languagefraction': 0.25,
                            'entropy_shannon': -0.0,
                            'frequency': '(0.0, 1.0)'},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'bit2languagefraction': 0.38864211955055483,
                            'entropy_shannon': 0.63651416829481278,
                            'frequency': '(0.66666666666666663, 0.33333333333333331)'},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'bit2languagefraction': 0.25,
                            'entropy_shannon': -0.0,
                            'frequency': '(1.0, 0.0)'}}}}}

        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # call frequency method
        d_calxargs = {}
        d_calxargs["o_max"] = 87
        d_calxargs["o_bin"] = "binary"
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.frequency, d_calxargs)
        # call entropy method
        d_calxargs = {}
        o_sequoia.set_leaf("frequency")
        o_sequoia.calx(calx.entropy_shannon, d_calxargs)
        # calx bit2languagefraction
        d_calxargs = {}
        o_sequoia.set_leaf("entropy_shannon")
        d_calxargs["o_bin"] = "binary"
        o_sequoia.calx(calx.bit2languagefraction, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    ### entropy ###
    def test_calx_shannon(self):
        dd_plum ={
            'cellline_b':{
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'value': [31, 40, 49],
                            'frequency': '(0.33333333333333331, 0.66666666666666663)',
                            'entropy_shannon': 0.63651416829481278},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'frequency': '(1.0, 0.0)',
                            'entropy_shannon': -0.0},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'frequency': '(0.0, 1.0)',
                            'entropy_shannon': -0.0}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # call frequency method
        d_calxargs = {}
        d_calxargs["o_max"] = 80
        d_calxargs["o_bin"] = "binary"
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.frequency, d_calxargs)
        # call entropy method
        d_calxargs = {}
        o_sequoia.set_leaf("frequency")
        o_sequoia.calx(calx.entropy_shannon, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_ginisimpson(self):
        dd_plum = {
            'cellline_b':{
                'run_b': {
                    'well_a': {
                        'nm_360': {
                            'value': [58, 67, 76],
                            'frequency': '(0.0, 1.0)',
                            'entropy_ginisimpson': 0.0},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'frequency': '(1.0, 0.0)',
                            'entropy_ginisimpson': 0.0},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'frequency': '(0.33333333333333331, 0.66666666666666663)',
                            'entropy_ginisimpson': 0.4444444444444444}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # call frequency method
        d_calxargs = {}
        d_calxargs["o_max"] = 80
        d_calxargs["o_bin"] = "binary"
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.frequency, d_calxargs)
        # call entrupy entropy
        d_calxargs = {}
        o_sequoia.set_leaf("frequency")
        o_sequoia.calx(calx.entropy_ginisimpson, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_raoexpression(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_120': {
                            'value': [4, 13, 22],
                            'frequency': '(1.0, 0.0)',
                            'entropy_raoexpression': 0.0},
                        'nm_360': {
                            'value': [58, 67, 76],
                            'frequency': '(0.0, 1.0)',
                            'entropy_raoexpression': 0.0},
                        'nm_240': {
                            'value': [31, 40, 49],
                            'frequency': '(0.33333333333333331, 0.66666666666666663)',
                            'entropy_raoexpression': 0.07407407407407407}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest00.json")
        o_sequoia.json_read()
        # call frequency method
        d_calxargs = {}
        d_calxargs["o_max"] = 80
        d_calxargs["o_bin"] = "binary"
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.frequency, d_calxargs)
        # call entropt method
        d_calxargs = {}
        o_sequoia.set_leaf("frequency")
        o_sequoia.calx(calx.entropy_raoexpression, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


    def test_calx_vocabulary(self):
        dd_plum = {
            'cellline_b': {
                'run_b': {
                    'well_a': {
                        'nm_240': {
                            'value': [
                                0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                                10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                                20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                                30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                                40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
                                50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
                                60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
                                70, 71, 72, 73, 74],
                            'vocabulary': [
                                -0.0, -0.0, -0.0, -0.0, -0.0, -0.0, -0.0, -0.0,
                                -0.0, -0.0, -0.0, -0.0, -0.0, -0.0, -0.0, -0.0]},
                        'nm_360': {
                            'value': [
                                0, 1, 2, 4, 8, 16, 32, 64,
                                128, 256, 512, 1024, 2048, 4096,
                                8192],
                            'vocabulary': [
                                -0.0, 0.24493002679463532, 0.24493002679463532, 0.48509409130221154,
                                0.48509409130221154, 0.48509409130221154, 0.48509409130221154, 0.72012488127180974,
                                0.72012488127180974, 0.72012488127180974, 0.72012488127180974, 0.72012488127180974,
                                0.72012488127180974, 0.72012488127180974, 0.72012488127180974, 0.94959366771673837]},
                        'nm_120': {
                            'value': [4, 13, 22],
                            'vocabulary': [-0.0, -0.0, -0.0, -0.0,
                                        -0.0, -0.0, -0.0, -0.0,
                                        -0.0, -0.0, -0.0, -0.0,
                                        -0.0, -0.0, -0.0, -0.0]}}}}}
        # get ddtree object
        o_sequoia = ddtree()
        o_sequoia.b_verbose = None
        o_sequoia.set_ifile("calx_smoketest01.json")
        o_sequoia.json_read()
        # call vocabulary method
        d_calxargs = {}
        d_calxargs["o_max"] = 16384
        d_calxargs["ti_binrange"] = (1,16)
        #d_calxargs["o_entropy"] = calx.entropy_ginisimpson
        o_sequoia.set_leaf("value")
        o_sequoia.calx(calx.vocabulary, d_calxargs)
        self.assertEqual(o_sequoia, dd_plum)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
