"""
title: pixie.py
language: python 3.5

author: bue

description:
    ddtree.py calx compatible plot objects library.

run:
"""
# python standard library

# python scipy library
import numpy as np
import matplotlib.pyplot as plt

# pwn


### constats ###
#ls_color = ["k","m","b","c","g","w","y","r"]
ls_color = ["k","m","r","g","w","y","c","b"]


### plots ###
def hist_value(o_ddtree, d_calxargs):
    """
    plots one s_value twigleaf as histogram
    plots ls_value twigleafs as legend
    b_png = True for png plots, False = screenoutput
    li_title = species the part of the branch that should be title
    if o_ddtree.b_development is true, only the first plot will be outputed
    r_ymax
    b_png
    li_title
    ls_legend
    s_xlabel
    s_colour
    """
    # extract d_calxargs
    d_leaf = d_calxargs["d_leaf"]
    ls_branch = d_calxargs["ls_branch"]

    # handle input
    # values
    lr_value = d_leaf[o_ddtree.s_value]

    # r_ymax
    try:
        r_ymax = d_calxargs["r_ymax"]
    except KeyError:
        r_ymax = max(lr_value)

    # b_png
    try:
        b_png = d_calxargs["b_png"]
    except KeyError:
        b_png = True

    # li_title
    try:
      li_title = d_calxargs["li_title"]
    except KeyError:
      li_title = [-1]
    # s_title
    ls_title = []
    for i_title in li_title:
        s_title = ls_branch[i_title]
        ls_title.append(s_title)
    s_title = ":".join(ls_title)

    # s_legend
    try:
      ls_legend = [s_measure + "=" +str(d_leaf[s_measure]) for s_measure in d_calxargs["ls_legend"]]
      s_legend = "\n".join(ls_legend)
    except KeyError:
      s_legend = None

    # s_xlabel
    try:
      s_xlabel =  d_calxargs["s_xlabel"]
    except KeyError:
      s_xlabel = None

    # s_colour
    try:
      s_colour = d_calxargs["s_colour"]
    except KeyError:
      s_colour = "k"

    # frame data
    o_fig = plt.figure()
    ax = o_fig.add_subplot(111)
    i_xmax = len(lr_value)
    ax.set_xlim(0, i_xmax)
    ax.set_ylim(0, r_ymax+r_ymax*0.03)
    ax.set_ylabel(o_ddtree.s_value)
    if (s_xlabel != None):
        ax.set_xlabel(s_xlabel)
    if (s_legend != None):
        plt.text(i_xmax*0.02, r_ymax*0.92-len(ls_legend)*0.01, s_legend, color="k")
    plt.title(s_title)

    # plot data
    ax.bar(height=lr_value, left=range(i_xmax), facecolor=s_colour, alpha=0.74)
    # plot png
    if (o_ddtree.b_png):
        if (o_ddtree.b_outta):
            s_out = "-".join(ls_branch)
            if (o_ddtree.s_ofile):
                s_out = o_ddtree.s_ofile+s_out + ".png"
            else:
                s_out = s_out + ".png"
            plt.savefig(s_out)
        if (o_ddtree.b_development):
            o_ddtree.b_outta = False
        b_out = None
    # plot screen
    else:
        if (o_ddtree.b_outta):
            plt.show()
        if (o_ddtree.b_development):
            o_ddtree.b_outta = False
        b_out = None
    # out to calx
    return(None)



def hist_leaf(o_ddtree, d_calxargs):
    """
    plots many s_value twigtwigleaf into one histogram
    plots the first twig as legend
    b_png = True for png plots, False = screenoutput
    li_title = species the part of the branch that should be title
    if o_ddtree.b_development is true, only the first plot will be outputed
    r_ymax
    b_png
    li_title
    s_xlabel
    s_ylabel = o_ddtree.s_value (!)
    ls_colour
    s_input = "" # what usualy the value is
    ls_value = dort wo self.s_value hit
    """
    # extract d_calxargs
    d_leaf = d_calxargs["d_leaf"]
    ls_branch = d_calxargs["ls_branch"]

    # handle input
    try:
        ls_value = d_calxargs["ls_value"]
    except:
        ls_value = list(d_leaf.keys())
        ls_value.sort()

    # s_input
    try:
        s_input = d_calxargs["s_input"]
    except KeyError:
        s_input = "value"
    # i_xmax
    i_xmax = len(d_leaf[ls_value[0]][s_input])

    print("TEARS:", d_leaf)

    # r_ymax
    try:
        r_ymax = d_calxargs["r_ymax"]
    except KeyError:
        r_ymax = 0.4

    # b_png
    try:
        b_png = d_calxargs["b_png"]
    except KeyError:
        b_png = True

    # li_title
    try:
      li_title = d_calxargs["li_title"]
    except KeyError:
      li_title = [-1]
    # s_title
    ls_title = []
    for i_title in li_title:
        s_title = ls_branch[i_title]
        ls_title.append(s_title)
    s_title = ":".join(ls_title)

    # s_legend
    try:
      i_legend = d_calxargs["i_legend"]
    except KeyError:
      i_legend = -1

    # s_xlabel
    try:
      s_xlabel =  d_calxargs["s_xlabel"]
    except KeyError:
      s_xlabel = None

    # s_colour
    try:
      ls_colour = d_calxargs["ls_colour"]
    except KeyError:
      ls_colour = ls_color

    # plt histogram
    o_fig = plt.figure()
    ax = o_fig.add_subplot(111)
    ax.set_ylim(0,r_ymax)
    ax.set_xlim(0,i_xmax)
    ax.set_ylabel(s_input)
    ax.set_xlabel(s_xlabel)
    plt.set_title(s_title)
    if (o_ddtree.b_verbose):
        print("plot:", s_title)

    # loop to data to plot
    ls_legend = []
    i = 0
    for s_value in ls_value:
        lr_value = d_leaf[s_value][s_input]
        print("Elmar:", s_value, lr_value, i_xmax, ls_colour[i])
        # make bars
        ax.bar(height=lr_value, left=range(i_xmax), facecolor=ls_colour[i], alpha=0.74)
        # make legend
        ls_legend.append(s_value)
        # next
        i += 1
    # put legend
    ax.legend(ls_legend, loc='upper left')

    # plot png
    if (o_ddtree.b_png):
        if (o_ddtree.b_outta):
            s_out = "-".join(ls_branch)
            if (o_ddtree.s_ofile):
                s_out = o_ddtree.s_ofile+s_out + ".png"
            else:
                s_out = s_out + ".png"
            plt.savefig(s_out)
        if (o_ddtree.b_development):
            o_ddtree.b_outta = False
        b_out = None
    # plot screen
    else:
        if (o_ddtree.b_outta):
            plt.show()
        if (o_ddtree.b_development):
            o_ddtree.b_outta = False
        b_out = None
    # out to calx
    return(b_out)


def hist_gate(o_ddtree, d_calxargs):
    """
    plots many s_value twigtwigleaf into one histogram
    plots ls_value twigleafs as legend
    b_png = True for png plots, False = screenoutput
    li_title = species the part of the branch that should be title
    if o_ddtree.b_development is true, only the first plot will be outputed
    r_ymax
    b_png
    li_title
    s_xlabel
    s_ylabel = o_ddtree.s_value (!)
    ls_colour
    s_input = "" # what usualy the value is
    ls_value = dort wo self.s_value hit
    """
    print("YEPPA!")
    # extract d_calxargs
    d_leaf = d_calxargs["d_leaf"]
    ls_branch = d_calxargs["ls_branch"]

    # handle input
    # s_input
    try:
        s_input = d_calxargs["s_input"]
    except KeyError:
        s_input = "speciesgate"


    # ls_value
    if (s_input != o_ddtree.s_value):
        # multicase
        print("ELMAR",ls_branch, o_ddtree.s_value, s_input, d_leaf[o_ddtree.s_value].keys())
        ts_label = d_leaf[o_ddtree.s_value][s_input]["ts_label"]
        ls_value = list(d_leaf.keys())
        ls_value.sort()
        # legend
        ls_legend = ls_value
        s_legend = None
        # ts_word
        ts_word = d_leaf[o_ddtree.s_value][s_input]["ts_word"]
        # r_ymax
        r_ymax = d_leaf[o_ddtree.s_value][s_input]["ts_major"][-1]
        # i_xmax
        #i_xmax = len(ts_word)
    else:
        # single case
        try:
            ts_label = d_leaf[o_ddtree.s_value]["ts_label"]
            ls_value = d_calxargs["ls_value"]
        except KeyError:
            ls_value = ["i_total","r_pmisfits","r_shannon","r_ginisimpson","r_rao" ]
        # legend
        print("FFFFF:", d_leaf[o_ddtree.s_value].keys())
        ls_legend = [s_measure + "=" + str(d_leaf[o_ddtree.s_value][s_measure]) for s_measure in ls_value]
        s_legend = "\n".join(ls_legend)
        ls_legend = None
        # ts_word
        ts_word = d_leaf[o_ddtree.s_value]["ts_word"]
        # r_ymax
        #r_ymax = d_leaf[o_ddtree.s_value]["ts_major"][-1]
        # i_xmax
        #i_xmax = len(ts_word)

    # li_title
    try:
        li_title = d_calxargs["li_title"]
    except KeyError:
        li_title = []
    # s_title
    ls_title = list(ts_label)
    for i_title in li_title:
        s_title = ls_branch[i_title]
        ls_title.append(s_title)
    s_title = " : ".join(ls_title)

    # b_png
    try:
        b_png = d_calxargs["b_png"]
    except KeyError:
        b_png = True

    # s_xlabel
    try:
        s_xlabel =  d_calxargs["s_xlabel"]
    except KeyError:
        s_xlabel = None

    # ls_colour
    try:
        ls_colour = d_calxargs["ls_colour"]
        if(type(ls_colour) == str):
            ls_colour = [ls_colour]
    except KeyError:
        try:
            s_colour = d_calxargs["s_colour"]
            ls_colour = [s_colour]
        except KeyError:
            ls_colour = ls_color

    # plt histogram
    o_fig = plt.figure()
    ax = o_fig.add_subplot(111)
    #ax.set_ylim(0,r_ymax)
    #ax.set_xlim(0,i_xmax)
    ax.set_ylabel(s_input + " freqency")
    if (s_xlabel != None):
        ax.set_xlabel(s_xlabel)
    ax.set_xticks(range(len(ts_word)))
    ax.xaxis.grid(color='gray', linestyle='dashed')
    if (len(ts_word) > 16):
        ax.set_xticklabels(ts_word, rotation=90)
    else:
        ax.set_xticklabels(ts_word)
    ax.set_ylim((0,1.4))
    ax.set_xlim((0,len(ts_word)))
    plt.title(s_title)
    if (o_ddtree.b_verbose):
        print("plot:", s_title)

    # loop through data to plot
    a_ind = np.arange(len(ts_word))
    if  (s_input != o_ddtree.s_value):
        # multicase
        r_width = 1 / (len(ls_value)+1)
        a_space = a_ind + r_width/2
        lo_legendcolor = []
        i = 0
        for s_value in ls_value:
            lr_value = []
            for s_word in ts_word:
                lr_value.append(d_leaf[s_value][s_input][s_word]/d_leaf[s_value][s_input]["i_total"])
            # make bars
            o_ax = ax.bar(left=a_space, height=lr_value, width=r_width, color=ls_colour[i])
            a_space = a_space + r_width
            lo_legendcolor.append(o_ax[0])
            i += 1
        # make legend
        ax.legend(lo_legendcolor, ls_value, loc='upper left')
    else:
        # singlecase
        a_space = a_ind
        lr_value = []
        for s_word in ts_word:
            lr_value.append(d_leaf[o_ddtree.s_value][s_word]/d_leaf[o_ddtree.s_value]["i_total"])
        # make bars
        print("EEEE:", lr_value)
        o_ax = ax.bar(left=a_space, height=lr_value, width=0.9, color=ls_colour[0])
        # plot legend
        plt.text(0.1, 1.06, s_legend, color="k")

    # plot png
    if (o_ddtree.b_png):
        if (o_ddtree.b_outta):
            s_out = s_input +"-"+ "-".join(ls_branch)
            if (o_ddtree.s_ofile):
                s_out = o_ddtree.s_ofile+s_out + ".png"
            else:
                s_out = s_out + ".png"
            plt.savefig(s_out)
        if (o_ddtree.b_development):
            o_ddtree.b_outta = False
        b_out = None
    # plot screen
    else:
        if (o_ddtree.b_outta):
            plt.show()
        if (o_ddtree.b_development):
            o_ddtree.b_outta = False
        b_out = None
    # out to calx
    return(None)
